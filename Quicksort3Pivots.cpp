#include "Quicksort3Pivots.h"
#include "Partitioner3Pivots.h"

Quicksort3Pivots::Quicksort3Pivots() : Quicksort(3)
{
	m_numberOfPivots = 3;
	m_numberOfSortingSmallerLists = new uint64_t[3-1];
	//set counter-array to 0
	Quicksort::resetNumberOfSortingSmallerLists();
	m_optCosts = new OptimalSortingCosts();
	m_optimalcosts = m_optCosts->getOptimalSortingCosts();
	
	m_partitioner = new Partitioner3Pivots();
}

Quicksort3Pivots::~Quicksort3Pivots()
{
	//base destructer gets called afterwards
}

