#ifndef CLASSIFICATIONSTRATEGY_H
#define CLASSIFICATIONSTRATEGY_H


#include "BinaryTree.h"



class ClassificationStrategy
{
private:
	uint64_t m_comparisons;
	int m_numberOfPivots;
	int m_numberOfLists;
	int m_numberOfTrees;
	std::vector<BinaryTree*>* m_trees = new std::vector<BinaryTree*>();
	int* m_heightsMatrix;  //(numberOfTrees)x(numberOfPivots+1)  last column: sum of prior entries
	uint64_t* m_lVector;
	
	void generateTrees();
	void generateMatrix();

	void updateVector(int index); //add col index to m_lVector
	
public:
	ClassificationStrategy(int numberPivots);
	~ClassificationStrategy();
	uint64_t getNumberOfComparisons();
	int getNumberOfPivots();
	std::vector<BinaryTree*>* getBinaryTrees();
	void reset(); //reset m_comparisons and m_lVector
	int64_t classifyElement(int64_t element, std::vector<int64_t>* pivots);
	int chooseNextTree();
	
	
	//methods for debugging
	std::string nextTree();
	std::string Vector();
	std::string Matrix();
	
	void printTrees();
	void printNextTree();
	void printVector();
	void printMatrix();
};

#endif // CLASSIFICATIONSTRATEGY_H
