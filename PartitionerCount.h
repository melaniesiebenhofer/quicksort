#ifndef PARTITIONERCOUNT_H
#define PARTITIONERCOUNT_H

#include "ClassificationStrategy.h"
#include "Partitioner.h"
#include "OptimalSortingCosts.h"


class PartitionerCount : public Partitioner{

private:
	int m_numberOfPivots;
	std::vector<int64_t>* m_pivots;
	ClassificationStrategy* m_strategy;
	int m_numberOfLists;
	std::vector<int64_t>** m_lists;
	uint64_t m_numberOfComparisons;
	uint64_t* m_numberOfSortingSmallerLists;
	
	//for optimal costs:
	OptimalSortingCosts* m_optCosts;
	boost::rational<uint64_t>* m_optimalcosts;
	
	
	void resetPivots();
	void choosePivots(int64_t* array, int64_t startIndex, int64_t endIndex);
	void resetLists();
	
	void resetNumberofSortingSmallerLists();
	
public:
	
	PartitionerCount(int numberOfPivots);
	~PartitionerCount();

public:
	//comparisons not integer!
	//int getNumberOfComparisonsOfPivots();
	//int getNumberOfComparisonsSmallList(int size);
	
	std::vector<int64_t>** partition(int64_t* array, int64_t startIndex, int64_t endIndex);
	std::vector<int64_t>* getPivots();
	
	uint64_t getNumberOfComparisons();
	uint64_t* getNumberOfSortingSmallerLists();
	boost::rational<uint64_t> getNumberOfComparisonsForSmallerLists();
	
	void resetCounter();
	
};

#endif // PARTITIONERCOUNT_H
