#ifndef PARTITIONER3PIVOTS_H
#define PARTITIONER3PIVOTS_H

#include "Partitioner.h" // Base class: Partitioner

class Partitioner3Pivots : public Partitioner
{
public:
	Partitioner3Pivots();
	~Partitioner3Pivots();

public:
	virtual uint64_t getNumberOfComparisons();
	virtual uint64_t* getNumberOfSortingSmallerLists();
	virtual std::vector<int64_t>* getPivots();
	virtual std::vector<int64_t>** partition(int64_t* array, int64_t startIndex, int64_t endIndex);
	virtual void resetCounter();
	uint64_t* getCounterUsageOfTrees();
	
private:
    int m_partitionCounter = 0;
	int m_numberOfLists = 4;
	int m_numberOfPivots = 3;
	std::vector<int64_t>** m_lists;
	std::vector<int64_t>* m_pivots;
	uint64_t m_numberOfComparisons;
	uint64_t* m_numberOfSortingSmallerLists;
	uint64_t* m_counterUsageOfTrees;
	
	inline void pushList0(int64_t elem);
	inline void pushList1(int64_t elem);
	inline void pushList2(int64_t elem);
	inline void pushList3(int64_t elem);
	
	  
	int m_sortingTree = 0;
	uint64_t m_lVector[5];//5 sorting trees for 3 pivots
};

#endif // PARTITIONER3PIVOTS_H
