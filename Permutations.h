#ifndef PERMUTATIONS_H
#define PERMUTATIONS_H

#include <stdint.h>

class Permutations
{
	
private:
	int64_t* m_array;
	int64_t m_arrayLength;
	
	bool m_start;
	int64_t m_i;
	int64_t* m_c;
	
	int64_t m_counter;
	
	void copyArray(int64_t* arr, int64_t* copy, int64_t arrLength); //copies arr to copy (length of both arrays must be arrLength)
	void swap(int64_t i, int64_t j); //swaps element i and element j in m_array
	
public:
	Permutations(int64_t* startArray, int64_t arrayLength);
	~Permutations();

	bool getNextPermutation(int64_t* arrayForNextPermutation); //if there is a next permutation: returns true and writes this
															// permutation into array
															// else: returns false
	int64_t getNumberOfGeneratedPermutations();	//returns how many permutations have already been generated
	

	
	

};

#endif // PERMUTATIONS_H
