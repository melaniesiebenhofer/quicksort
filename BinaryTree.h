#ifndef BINARYTREE_H
#define BINARYTREE_H

#include "Node.h"
#include <iostream>
#include <string>
#include <map>

class BinaryTree
{
private:
	Node* m_top;
	BinaryTree* m_left;
	BinaryTree* m_right;
	
	//int m_comparisons; 
	
	void helpHeights(BinaryTree* tree, int height, std::map<int64_t,int>* heights);
	//int helpInsertElement(int value, std::vector<int>* pivots, int* comparisons);
	

public:
	~BinaryTree();
	BinaryTree(Node* top, BinaryTree* left, BinaryTree* right);
	Node* getRoot();
	void setRoot(Node* root);
	BinaryTree* getLeft();
	void setLeft(BinaryTree* left);
	BinaryTree* getRight();
	void setRight(BinaryTree* right);
	std::string getRepresentation();
	std::string getRepresentationWithLists();
	std::vector<int64_t> getKeys();
	BinaryTree* copy();
	std::map<int64_t,int>* getHeights();
	void increaseKeys(int inc);
	void increaseIndices(int inc);
	int64_t insertElement(int64_t value, std::vector<int64_t>* pivots);
	//int getNumberOfComparisons();

};

#endif // BINARYTREE_H
