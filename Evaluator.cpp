/*
 * Evaluator.cpp
 *
 *  Created on: 28.01.2018
 *      Author: melanie
 */

#include "Evaluator.h"

Evaluator::~Evaluator() {
	// TODO Auto-generated destructor stub
}
Evaluator::Evaluator() {
	// TODO Auto-generated constructor stub

}

void Evaluator::write_nr_of_comp_sort(int nPivots, int n, int64_t len){
	Quicksort* quicksorter = new Quicksort(nPivots);
		uint64_t count = 0;
		boost::rational<uint64_t> count2 = 0;

		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::default_random_engine g(seed);
		//int len = 100;

		int64_t* arr = new int64_t[len];
		for(int64_t i = 0; i < len; i++){
			arr[i] = len-i; //i+1;
		}

		std::ofstream myfile;
	    std::string filename = "Results/random_lists_size_";
	    filename += std::to_string(len);
	    filename += "_pivots_";
	    filename += std::to_string(nPivots);
	    filename += ".txt";
		myfile.open(filename);
		myfile << "pivots" << ";" << "len" << ";" << "count1" << ";" <<  "count2" <<"\n";

		for(int i = 0; i<n; i++){
			std::shuffle(&arr[0], &arr[len],g);
			count = quicksorter->sort(arr, len);
			count2 = quicksorter->getNumberOfComparisonsForSmallerLists();
			myfile << quicksorter->getNumberOfPivots() << ";"<< len << ";"  << count << ";" <<  count2 <<"\n";
			//std::cout << i << "\n";
		}

		myfile.close();
		std::cout << "finished\n";

		delete [] arr;
		delete quicksorter;
}

void Evaluator::write_avg_nr_of_comp_sort(int nPivots, int n, int64_t len, int reps){
	Quicksort* quicksorter = new Quicksort(nPivots);
	//uint64_t count = 0;
	boost::rational<uint64_t> count = 0;
	boost::rational<uint64_t> avg = 0;

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine g(seed);

	std::srand(unsigned(std::time(0))); //for std::random_shuffle
	//int len = 100;

	int64_t* arr = new int64_t[len];
	for(int64_t i = 0; i < len; i++){
		arr[i] = len-i; //i+1;
	}

	std::ofstream myfile;
    std::string filename = "Results/random_lists_avg_size_";
    filename += std::to_string(len);
    filename += "_pivots_";
    filename += std::to_string(nPivots);
    filename += ".txt";
	myfile.open(filename);

	myfile << "pivots" << ";" << "len" << ";" << "n" << ";" <<  "avg" <<"\n";

	for(int j = 0; j<reps; j++){
		avg = 0;
		for(int i = 0; i<n; i++){
			count = 0;
			//std::shuffle(&arr[0], &arr[len],g);
			std::random_shuffle(&arr[0], &arr[len]);
			count += quicksorter->sort(arr, len);
			count += quicksorter->getNumberOfComparisonsForSmallerLists();
            //avg += boost::rational<uint64_t>(count, n);
			avg += count/n;
		}
		myfile << quicksorter->getNumberOfPivots() << ";"<< len << ";"  << n << ";" <<  avg <<"\n";

		//std::cout << j << "\n";
	}

	myfile.close();
	std::cout << "finished\n";

	delete [] arr;
	delete quicksorter;
}

void Evaluator::write_avg_nr_of_comp_sort_3_pivots(int n, int64_t len, int reps){
    Quicksort3Pivots* quicksorter = new Quicksort3Pivots();
	//uint64_t count = 0;
	boost::rational<uint64_t> count = 0;
	boost::rational<uint64_t> avg = 0;

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine g(seed);

	int64_t* arr = new int64_t[len];
	for(int64_t i = 0; i < len; i++){
		arr[i] = i; //i+1;
	}

	std::vector<int64_t> a;
	//std::cout << "max size vector = " << a.max_size() << std::endl;

	std::ofstream myfile;
    std::string filename = "Results/random_lists_avg_size_";
    filename += std::to_string(len);
    filename += "_pivots_3";
    filename += ".txt";
	myfile.open(filename);

	myfile << "pivots" << ";" << "len" << ";" << "n" << ";" <<  "avg" <<"\n";

	for(int j = 0; j<reps; j++){
		avg = 0;
		for(int i = 0; i<n; i++){
			count = 0;
			std::shuffle(&arr[0], &arr[len],g);
			std::cout << "shuffled list" << std::endl;
			count += quicksorter->sort(arr, len);
			count += quicksorter->getNumberOfComparisonsForSmallerLists();
            //avg += boost::rational<uint64_t>(count, n);
			avg += count/n;
			//std::cout << "finished " << i << std::endl;
		}
		myfile << quicksorter->getNumberOfPivots() << ";"<< len << ";"  << n << ";" <<  avg <<"\n";

		//std::cout << j << "\n";
	}

	myfile.close();
	std::cout << "finished" << std::endl;

	delete [] arr;
	delete quicksorter;
}

void Evaluator::write_avg_nr_of_comp_partition(int nPivots, int n, int64_t len){
	PartitionerCount* partitioner = new PartitionerCount(nPivots);
	uint64_t count = 0;
	boost::rational<uint64_t> avg = 0;

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine g(seed);

	int64_t* arr = new int64_t[len];
	for(int64_t i = 0; i < len; i++){
		arr[i] = len-i; //i+1;
	}

	std::ofstream myfile;
    std::string filename = "Results/random_lists_partition_size_";
    filename += std::to_string(len);
    filename += "_pivots_";
    filename += std::to_string(nPivots);
    filename += ".txt";
	myfile.open(filename);
	myfile << "pivots" << ";" << "len" << ";" << "count1" << ";"   << "avg"  <<"\n";

	for(int i = 0; i<n; i++){
		std::shuffle(&arr[0], &arr[len],g);
		partitioner->partition(arr, 0, len-1);
		count = partitioner->getNumberOfComparisons();
        avg = boost::rational<uint64_t>(count, len-nPivots);
		myfile << nPivots << ";"<< len << ";"  << count << ";" <<  avg <<"\n";
		partitioner->resetCounter();
		//std::cout << i << "\n";
	}

	myfile.close();
	std::cout << "finished" << std::endl;

	delete [] arr;
	delete partitioner;
}


void Evaluator::write_avg_nr_of_comp_partition_3_pivots(int n, int64_t len){
	Partitioner3Pivots* partitioner = new Partitioner3Pivots();
	uint64_t count = 0;
	boost::rational<uint64_t> avg = 0;

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine g(seed);

	int64_t* arr = new int64_t[len];
	for(int64_t i = 0; i < len; i++){
		arr[i] = len-i; //i+1;
	}

	std::ofstream myfile;
    std::string filename = "Results/random_lists_partition_size_";
    filename += std::to_string(len);
    filename += "_pivots_3";
    filename += ".txt";

	myfile.open(filename);
	myfile << "pivots" << ";" << "len" << ";" << "count" << ";"   << "avg"  <<"\n";

	for(int i = 0; i<n; i++){
		std::shuffle(&arr[0], &arr[len], g);
		partitioner->partition(arr, 0, len-1);
		count = partitioner->getNumberOfComparisons();
		avg = boost::rational<uint64_t>(count,len-3);
        //avg = count/(len-3);
		myfile << 3 << ";"<< len << ";"  << count << ";" <<  avg <<"\n";
		partitioner->resetCounter();
		//std::cout << i << "\n";
	}

	myfile.close();
	std::cout << "finished\n";

	delete [] arr;
	delete partitioner;
}

void Evaluator::write_nr_of_comp_to_sort_all_possible_permutations(int nPivots, int64_t len){

	int64_t* arr = new int64_t[len];
	for(int64_t i = 0; i < len; i++){
		arr[i] = i + 1;
	}

	int64_t* b = new int64_t[len];
	uint64_t count = 0;
	boost::rational<uint64_t> count2 = 0;

	Quicksort* quicksorter = new Quicksort(nPivots);
	Permutations* perm = new Permutations(arr, len);


	while(perm->getNextPermutation(b)){
		count += quicksorter->sort(b,len);
		count2 += quicksorter->getNumberOfComparisonsForSmallerLists();
		//std::cout << perm->getNumberOfGeneratedPermutations() << ": " << count << " + " << count2 << "\n";
	}

	std::ofstream myfile;
	std::string filename = "Results/sort_all_permutations_of_len_";
	filename += std::to_string(len);
    filename += "_pivots";
    filename += std::to_string(nPivots);
    filename += ".txt";
	myfile.open(filename);

	myfile << "\n" << "# of comparisons: "<< count << "+" << count2 << " = " << count2 + count;

	myfile.close();
	delete quicksorter;
	delete perm;
	delete [] arr;
	delete [] b;
}

void Evaluator::write_nr_of_comp_to_sort_all_possible_permutations_3_pivots(int64_t len){

	int64_t* arr = new int64_t[len];
	for(int64_t i = 0; i < len; i++){
		arr[i] = i + 1;
	}

	int64_t* b = new int64_t[len];
	uint64_t count = 0;
	boost::rational<uint64_t> count2 = 0;

	Quicksort* quicksorter = new Quicksort3Pivots();
	Permutations* perm = new Permutations(arr, len);


	while(perm->getNextPermutation(b)){
		count += quicksorter->sort(b,len);
		count2 += quicksorter->getNumberOfComparisonsForSmallerLists();
		//std::cout << perm->getNumberOfGeneratedPermutations() << ": " << count << " + " << count2 << "\n";
	}

	std::ofstream myfile;
	std::string filename = "Results/sort_all_permutations_";
	filename += std::to_string(len);
    filename += "_pivots_3";
    filename += ".txt";
	myfile.open(filename);

	myfile << "\n" << "# of comparisons: "<< count << "+" << count2 << " = " << count2 + count << std::endl;

	myfile.close();
	delete quicksorter;
	delete perm;
	delete [] arr;
	delete [] b;
}

void Evaluator::write_tree_usage_partition_3_pivots(int n, int64_t len){
	Partitioner3Pivots* partitioner = new Partitioner3Pivots();
	uint64_t* counter;
	boost::rational<uint64_t> avg = 0;
	std::vector<int64_t>* pivots;

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine g(seed);

	int64_t* arr = new int64_t[len];
	for(int64_t i = 0; i < len; i++){
		arr[i] = len-i; //i+1;
	}

	std::ofstream myfile;
    std::string filename = "Results/random_lists_trees_size_";
    filename += std::to_string(len);
    filename += "_pivots_3";
    filename += ".txt";

	myfile.open(filename);
	myfile << "pivots" << ";" << "len" << ";" << "tree1;tree2;tree3;tree4;tree5;"<< "avg;" << "pivot1;pivot2;pivot3" <<"\n";

	for(int i = 0; i<n; i++){
		std::shuffle(&arr[0], &arr[len], g);
		partitioner->partition(arr, 0, len-1);
		counter = partitioner->getCounterUsageOfTrees();
		avg = boost::rational<uint64_t>(partitioner->getNumberOfComparisons(),len-3);
		pivots = partitioner->getPivots();
		myfile << 3 << ";"<< len << ";"  << counter[0] << ";" <<  counter[1] << ";" << counter[2] << ";" << counter[3] << ";" << counter[4] << ";" << avg << ";";
		myfile << pivots->at(0) << ";" << pivots->at(1) << ";" << pivots->at(2) << "\n";
		partitioner->resetCounter();
		//std::cout << i << "\n";
	}

	myfile.close();
	std::cout << "finished \n";

	delete [] arr;
	delete partitioner;
}
