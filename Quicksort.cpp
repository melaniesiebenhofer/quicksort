#include "Quicksort.h"
//#include <vector>
//#include "Partitioner.h"
#include "PartitionerCount.h"
//#include "OptimalSortingCosts.h"

Quicksort::Quicksort(int numberOfPivots)
{
	m_numberOfPivots = numberOfPivots;
	m_numberOfSortingSmallerLists = new uint64_t[numberOfPivots-1];
	//set counter-array to 0
	resetNumberOfSortingSmallerLists();
	m_optCosts = new OptimalSortingCosts();
	m_optimalcosts = m_optCosts->getOptimalSortingCosts();
	
	m_partitioner = new PartitionerCount(numberOfPivots);
}

Quicksort::~Quicksort()
{
	delete m_optCosts;
	delete m_numberOfSortingSmallerLists;
	//delete m_partitioner;
}

uint64_t Quicksort::sort(int64_t* array, int64_t arrayLength){ //returns # of key comparisons
	//resets counter and list for less entries than #pivots
	m_partitioner->resetCounter();
	resetNumberOfSortingSmallerLists();
	
	//sort
	helpSort(array, 0, arrayLength-1);
	
	//evaluate #comparisons
	uint64_t* comp = m_partitioner->getNumberOfSortingSmallerLists();
	for(int i = 0; i<m_numberOfPivots-1; i++){
		m_numberOfSortingSmallerLists[i] += comp[i];
	}
	
	return m_partitioner->getNumberOfComparisons();
}

int Quicksort::getNumberOfPivots(){
	return m_numberOfPivots;
}

void Quicksort::helpSort(int64_t* array, int64_t startIndex, int64_t endIndex){ //sort subarray [startindex, endindex]
	if(/*startIndex != endIndex  && */ endIndex > startIndex){
		noPivots = false; //if size of list was too small --> no pivots
		//int listSizes[] = int[m_numberOfPivots+1];
		std::vector<int64_t>* listSizes = new std::vector<int64_t>(m_numberOfPivots+1);
		std::vector<int64_t>** lists = m_partitioner->partition(array, startIndex, endIndex);
		std::vector<int64_t>* pivots = m_partitioner->getPivots();
		if(pivots->size() == 0){
			noPivots = true;
		}
		int64_t index = startIndex;
		std::vector<int64_t>::iterator it;
		for(int i = 0; i<m_numberOfPivots; i++){
			for(it = lists[i]->begin(); it!=lists[i]->end(); it++){ //writeback entries of list i
				array[index] = *it; //std::cout << *it << " ";
				index++;
			}
			
			//listSizes[i] = lists[i]->size();
			listSizes->at(i) = lists[i]->size();
			if(!noPivots){ //case listelements < pivots --> partitioner returns only one list and no pivots
				array[index] = pivots->at(i); //std::cout <<  " | "<< pivots->at(i) << " | ";
				index++;
			}
		}
		//last list (without pivot at end):
		for(it = lists[m_numberOfPivots]->begin(); it!=lists[m_numberOfPivots]->end(); it++){
			array[index] = *it; //std::cout << *it << " ";
			index++;
		}
		//listSizes[m_numberOfPivots] = lists[m_numberOfPivots]->size();
		listSizes->at(m_numberOfPivots) = lists[m_numberOfPivots]->size();
		
		//std::cout << "\n";
		
		//sort subarrays:
		index = startIndex;
//		index = endIndex; //NEW
		if(!noPivots){
			for(int i = 0; i<m_numberOfPivots+1; i++){
				if(listSizes->at(0)!=0){
					//std::cout << "sort from index " << index << " to " << index + listSizes->at(i) - 1 << "\n";
					helpSort(array, index, index+listSizes->at(0)-1);
				}
				index += listSizes->at(0) + 1; //+1 for pivot
				listSizes->erase(listSizes->begin());
			}
//			for(int i = 0; i < m_numberOfPivots+1; i++){									//NEW
//				if(listSizes->back()!= 0){ //if size of list (begin from back) != 0					//NEW
//					//std::cout << "sort from index " << index << " to " << index + listSizes->at(i) - 1 << "\n";	//NEW
//					helpSort(array, index - listSizes->back() + 1, index);						//NEW
//				}													//NEW
//				index -= listSizes->back() + 1; //decrease index (+1 for pivot)						//NEW
//				listSizes->pop_back(); //remove size of sorted list from list of sizes					//NEW
//			}														//NEW
		}
		delete listSizes;
	}
}

uint64_t* Quicksort::getNumberOfSortingSmallerLists(){
	return m_numberOfSortingSmallerLists;
}

boost::rational<uint64_t> Quicksort::getNumberOfComparisonsForSmallerLists(){
	uint64_t* numberComparisons = getNumberOfSortingSmallerLists();
	//Rat optimalComparisons[] = {Rat(1,1), Rat(16,6), Rat(112,24), Rat(832, 120), Rat(6896, 720) /*lower bound*/ }; 

	if(m_numberOfPivots < 2 || m_numberOfPivots > m_optCosts->getSizeOfOptimalSortingCostsArray()+1 /* = 6 */){ //for #pivots>6 no results
		return 0;
	} else {
		boost::rational<uint64_t> sum = boost::rational<uint64_t>(0,1);
		for(int i = 0; i < m_numberOfPivots-1; i++){
			//sum =  sum + optimalcosts[i]*boost::rational<uint64_t>(numberComparisons[i],1);
			sum =  sum + m_optimalcosts[i]*numberComparisons[i];
			//std::cout << "\n" << "sum = " << sum.numerator<<"/"<<sum.denominator << " + " << (optimalComparisons[i]).numerator <<"/" << (optimalComparisons[i]).denominator << "*" << numberComparisons[i];
		}
		//std::cout << "sum: " << sum.numerator << "/" << sum.denominator << "\n";
		//return boost::rational_cast<long double>(sum);
		return sum;
	}
}

void Quicksort::resetNumberOfSortingSmallerLists(){
	if(m_numberOfPivots>1){
		 for(int i = 0; i<m_numberOfPivots-1; i++){  //eg: numberOfPivots = 5:  List:  |2|3|4|5| (#comp 1 element = 0)
			 m_numberOfSortingSmallerLists[i] = 0;
		 }		 
	}
}
