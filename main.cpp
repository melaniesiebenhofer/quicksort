#include <stdio.h>
#include "Evaluator.h"
#include <iostream>
#include <cmath> //std::pow
#include <string> //std::stoi
//#include <boost/rational.hpp>



int main(int argc, char **argv)
{
	int method;
	if(argc < 2){
		std::cerr << "too few arguments passed" << std::endl;
		return -1;
	}
	else{
		method = std::stoi(argv[1]);
	}
	int nPivots, lower, upper, reps, n;

	try{
		switch(method){
			case 1:
			{
				if(argc < 6){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				nPivots = std::stoi(argv[2]);
				lower = std::stoi(argv[3]);
				upper = std::stoi(argv[4]);
				n = std::stoi(argv[5]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_nr_of_comp_sort(nPivots, n, std::pow(2,i));
				}
				break;
			}
			case 2:
			{
				if(argc < 7){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				nPivots = std::stoi(argv[2]);
				lower = std::stoi(argv[3]);
				upper = std::stoi(argv[4]);
				n = std::stoi(argv[5]);
				reps = std::stoi(argv[6]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_avg_nr_of_comp_sort(nPivots, n, std::pow(2,i), reps);
				}
				break;
			}
			case 3:
			{
				if(argc < 6){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				lower = std::stoi(argv[2]);
				upper = std::stoi(argv[3]);
				n = std::stoi(argv[4]);
				reps = std::stoi(argv[5]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_avg_nr_of_comp_sort_3_pivots(n, std::pow(2,i), reps);
				}
				break;
			}
			case 4:
			{
				if(argc < 6){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				nPivots = std::stoi(argv[2]);
				lower = std::stoi(argv[3]);
				upper = std::stoi(argv[4]);
				n = std::stoi(argv[5]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_avg_nr_of_comp_partition(nPivots, n, std::pow(2,i));
				}
				break;
			}
			case 5:
			{
				if(argc < 5){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				lower = std::stoi(argv[2]);
				upper = std::stoi(argv[3]);
				n = std::stoi(argv[4]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_avg_nr_of_comp_partition_3_pivots(n, std::pow(2,i));
				}
				break;
			}
			case 6:
			{
				if(argc < 5){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				nPivots = std::stoi(argv[2]);
				lower = std::stoi(argv[3]);
				upper = std::stoi(argv[4]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_nr_of_comp_to_sort_all_possible_permutations(nPivots, i);
				}
				break;
			}
			case 7:
			{
				if(argc < 4){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				lower = std::stoi(argv[2]);
				upper = std::stoi(argv[3]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_nr_of_comp_to_sort_all_possible_permutations_3_pivots(i);
				}
				break;
			}
			case 8:
			{
				if(argc < 5){
					std::cerr << "too few arguments passed" << std::endl;
					return -1;
				}
				lower = std::stoi(argv[2]);
				upper = std::stoi(argv[3]);
				n = std::stoi(argv[4]);
				for(int i = lower; i <= upper; i++){
					Evaluator::write_tree_usage_partition_3_pivots(n, std::pow(2,i));
				}
				break;
			}
			default:
			{
				std::cerr << "first argument invalid please read the README file" << std::endl;
				return -1;
			}
		}
	}
	catch(const std::exception & ex ){
		std::cerr << ex.what() << std::endl;
	}
	


	return 0;
}

