#include "ClassificationStrategy.h"
//#include "Node.h"
//#include "BinaryTree.h"
//#include <iostream>
//#include <string>
//#include <map>
#include <limits>
//#include <string>



ClassificationStrategy::ClassificationStrategy(int numberPivots)
{
	m_comparisons = 0;
	m_numberOfPivots = numberPivots;
	m_numberOfLists = m_numberOfPivots + 1;
	generateTrees();
	m_numberOfTrees = m_trees->size();
	m_heightsMatrix = new int[m_numberOfTrees*(m_numberOfLists + 1)];
	m_lVector = new uint64_t[m_numberOfTrees];
	generateMatrix();
	
}

ClassificationStrategy::~ClassificationStrategy()
{
	delete m_trees;
	delete [] m_heightsMatrix;
	delete [] m_lVector;
}

uint64_t ClassificationStrategy::getNumberOfComparisons(){
	return m_comparisons;
}

int ClassificationStrategy::getNumberOfPivots(){
	return m_numberOfPivots;
}

void ClassificationStrategy::generateTrees(){
	std::vector<BinaryTree*>* subTrees[getNumberOfPivots()+1];
	subTrees[0] = new std::vector<BinaryTree*>{new BinaryTree(new Node(0,false), nullptr, nullptr)};
	for(int i = 1; i<=getNumberOfPivots(); i++){
		subTrees[i] = new std::vector<BinaryTree*>();
	}
	BinaryTree* newTree;
	std::vector<BinaryTree*>::iterator itLeft;
	std::vector<BinaryTree*>::iterator itRight;
	
	for(int j = 1; j<=getNumberOfPivots(); j++){
		for(int i = 1; i <= j; i++){
			newTree = new BinaryTree(new Node(i-1,true), nullptr, nullptr); //root
			for(itLeft = subTrees[i-1]->begin(); itLeft != subTrees[i-1]->end(); itLeft++){
				for(itRight = subTrees[j-i]->begin(); itRight != subTrees[j-i]->end(); itRight++){
					newTree->setLeft((*itLeft)->copy());
					newTree->setRight((*itRight)->copy());
					newTree->getRight()->increaseIndices(i);
					newTree->getRight()->increaseKeys(i);
					subTrees[j]->push_back(newTree->copy());
					//std::cout << "tree "  << newTree->getRepresentation() << " added \n";
					newTree->setLeft(nullptr);
					newTree->setRight(nullptr);
				}
			}
		}
	}
	
	
	for(itRight = (subTrees[getNumberOfPivots()])->begin(); itRight != (subTrees[getNumberOfPivots()])->end(); itRight++){
		m_trees->push_back((*itRight)/*->copy()*/);
	}
}

std::vector<BinaryTree*>* ClassificationStrategy::getBinaryTrees(){
	return m_trees;
}

void ClassificationStrategy::generateMatrix(){
	std::map<int64_t,int>* row = new std::map<int64_t,int>();
	std::map<int64_t,int>::iterator it2;
	std::vector<BinaryTree*>::iterator it;
	int rowSum = 0;
	int i = 0;
	int j = 0;
	
	for(it = m_trees->begin(); it!=m_trees->end(); it++){
		row = (*it)->getHeights();
		rowSum = 0; //reset rowSum
		for(it2 = row->begin(); it2!=row->end(); it2++){
			m_heightsMatrix[i] = it2->second;
			rowSum += it2->second;
			i++;
			//std::cout << it2->second << " ";
		}
		m_heightsMatrix[i] = rowSum; //last entry in row
		m_lVector[j] = rowSum;
		//std::cout <<", " << rowSum << "; " << "\n";
		i++;
		j++;
	}
}

void ClassificationStrategy::reset(){
	m_comparisons = 0;
	
	//reset l-vector
	int rows = m_numberOfTrees;
	int cols = m_numberOfLists + 1;
	for(int i = 1; i<=rows; i++){
		m_lVector[i-1] = m_heightsMatrix[i*cols-1];
		//std::cout << i*cols-1 << ": " << m_heightsMatrix[i*cols-1] << "\n";
	}
	
}

int ClassificationStrategy::chooseNextTree(){
	int index = 0;
	long long min = std::numeric_limits<long long>::max();
	for(int i = 0; i < m_numberOfTrees; i++){
		if(m_lVector[i] < min){
			min = m_lVector[i];
			index = i;
		}
	}
	return index;
}

int64_t ClassificationStrategy::classifyElement(int64_t element, std::vector<int64_t>* pivots){
	int treeIndex = chooseNextTree();
	BinaryTree* sortingTree = m_trees->at(treeIndex);
	
	int64_t listIndex = sortingTree->insertElement(element, pivots);
	updateVector(listIndex); 
	
	//get number of comparisons from heightsMatrix
	int comp = m_heightsMatrix[(m_numberOfLists+1)*treeIndex + listIndex];
	m_comparisons += comp;
	
	return listIndex;
	
}

void ClassificationStrategy::updateVector(int index){
	//int rows = m_numberOfTrees;
	//int cols = m_numberOfLists + 1;
	for(int j = 0; j<m_numberOfTrees; j++){
		m_lVector[j] += m_heightsMatrix[j*(m_numberOfLists+1) + index];
	}
}


void ClassificationStrategy::printTrees(){
	int i = 0;
	for(std::vector<BinaryTree*>::iterator it = m_trees->begin(); it != m_trees->end(); it++){
		std::cout << "tree " << i <<": " << (*it)->getRepresentationWithLists() << "\n";
		i++;
	}
}


void ClassificationStrategy::printNextTree(){
	int i = chooseNextTree();
	std::cout << "next tree: " << i << "\n";
}



void ClassificationStrategy::printVector(){
	std::cout << "l-Vector: ";
	for(int j = 0; j<m_numberOfTrees; j++){
		std::cout << m_lVector[j] << ", ";
	}
	std::cout << "\n";
}


void ClassificationStrategy::printMatrix(){
	int rows = m_numberOfTrees;
	int cols = m_numberOfLists + 1;
	int j;
	std::cout << "Matrix: \n";
	for(int i = 0; i < rows; i++){
		for(j = 0; j < cols; j++){
			std::cout << m_heightsMatrix[i*cols + j] << " ";
		}
		std::cout << "\n";
	}
}

std::string ClassificationStrategy::nextTree(){
	int i = chooseNextTree();
	return m_trees->at(i)->getRepresentationWithLists();
}

std::string ClassificationStrategy::Vector(){
	std::string v("{");
	for(int j = 0; j<m_numberOfTrees; j++){
		v += std::to_string(m_lVector[j]);
		if(j != m_numberOfTrees-1){
			v += ", ";
		}
	}
	v += "}";
	return v;
}
std::string ClassificationStrategy::Matrix(){
	int rows = m_numberOfTrees;
	int cols = m_numberOfLists + 1;
	int j;
	std::string v("{");
	for(int i = 0; i < rows; i++){
		v += "{";
		for(j = 0; j < cols; j++){
			v += std::to_string(m_heightsMatrix[i*cols + j]);
			if(j != cols-1){
				v += ", ";
			}
		}
		v += "}";
		if(i != rows-1){
			v += ", ";
		}
	}
	v += "}";
	
	return v;
	
}