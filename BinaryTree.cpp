#include "BinaryTree.h"
#include <sstream>


BinaryTree::BinaryTree(Node* top, BinaryTree* left, BinaryTree* right)
{
	m_top = top;
	m_left = left;
	m_right = right;
}

BinaryTree::~BinaryTree() 
{
	delete m_top;
	delete m_left;
	delete m_right;
}

Node* BinaryTree::getRoot(){
	return m_top;
}

void BinaryTree::setRoot(Node* root){
	m_top = root;
}

BinaryTree* BinaryTree::getLeft(){
	return m_left;
}

void BinaryTree::setLeft(BinaryTree* left){
	m_left = left;
}

BinaryTree* BinaryTree::getRight(){
	return m_right;
}

void BinaryTree::setRight(BinaryTree* right){
	m_right = right;
}

std::string BinaryTree::getRepresentation(){
	std::stringstream ss;
	if(getLeft() != nullptr){
		ss << "(" << getLeft()->getRepresentation() << ")";
	}
	//ss << "(";
	if(getRoot()->isKey()){
		ss << getRoot()->getKey();
	}
	//ss << ")";
	if(getRight() != nullptr){
		ss << "(" << getRight()->getRepresentation() << ")";
	}
	std::string s = ss.str();
	return s;
}

std::string BinaryTree::getRepresentationWithLists(){
	std::stringstream ss;
	if(getLeft() != nullptr){
		ss << "(" << getLeft()->getRepresentationWithLists() << ")";
	}
	//ss << "(";
	if(getRoot()->isKey()){
		ss << getRoot()->getKey();
	} else {
		ss << "L" << getRoot()->getIndex();
	}
	//ss << ")";
	if(getRight() != nullptr){
		ss << "(" << getRight()->getRepresentationWithLists() << ")";
	}
	std::string s = ss.str();
	return s;
}

std::vector<int64_t> BinaryTree::getKeys(){
	std::string s = getRepresentation();
	std::vector<int64_t> keys;
	std::size_t pos = 0;
	std::size_t newpos;
	std::string temp;
	while(pos != s.length()){
		newpos = s.find_first_of("( )",pos);
		temp = s.substr(pos,newpos-pos);
		if(temp != ""){
			keys.push_back(std::stoll(temp));
		}
		if(pos != std::string::npos){
			pos += 1 + newpos-pos;
		}
	}
	return keys;
}


BinaryTree* BinaryTree::copy(){
	BinaryTree* newTree = new BinaryTree(getRoot()->copy(), nullptr, nullptr);
	BinaryTree* left = getLeft();
	BinaryTree* right = getRight();
	if(getLeft()!=nullptr){
		newTree->setLeft(left->copy());
	}
	if(getRight()!=nullptr){
		newTree->setRight(right->copy());
	}
	return newTree;
}

std::map<int64_t,int>* BinaryTree::getHeights(){
	//returns map with entries: <index, height>
	std::map<int64_t,int>* heights = new std::map<int64_t,int>();
	int64_t index;
	int height;
	if(getRoot()->isIndex()){
		index = getRoot()->getIndex();
		height = 0;
		heights->insert(std::make_pair(index,height));
	}
	if(getLeft()!=nullptr){
		helpHeights(getLeft(), 1, heights);
	}
	if(getRight()!=nullptr){
		helpHeights(getRight(), 1, heights);
	}
	return heights;
}

void BinaryTree::helpHeights(BinaryTree* tree, int height, std::map<int64_t,int>* heights){
	//inserts heights for heights > 0 into heigths
	int64_t index;
	if(tree->getRoot()->isIndex()){
		index = tree->getRoot()->getIndex();
		heights->insert(std::make_pair(index,height));
	}
	if(tree->getLeft()!=nullptr){
		helpHeights(tree->getLeft(), height+1, heights);
	}
	if(tree->getRight()!=nullptr){
		helpHeights(tree->getRight(), height+1, heights);
	}
}

void BinaryTree::increaseKeys(int inc){
	if(getRoot()->isKey()){
		getRoot()->setKey(getRoot()->getKey() + inc);
	}
	if(getLeft() != nullptr){
		getLeft()->increaseKeys(inc);
	}
	if(getRight() != nullptr){
		getRight()->increaseKeys(inc);
	}
}

void BinaryTree::increaseIndices(int inc){
	if(getRoot()->isIndex()){
		getRoot()->setIndex(getRoot()->getIndex() + inc);
	}
	if(getLeft() != nullptr){
		getLeft()->increaseIndices(inc);
	}
	if(getRight() != nullptr){
		getRight()->increaseIndices(inc);
	}
}



//commented code is for counting comparisons in class BinaryTree
int64_t BinaryTree::insertElement(int64_t value, std::vector<int64_t>* pivots){ 
	//m_comparisons = 0;
	if(getRoot()->isKey()){
		//m_comparisons++;
		if(value <= (*pivots)[getRoot()->getKey()]){ 
			return getLeft()->insertElement(value, pivots);
		} else {
			return getRight()->insertElement(value, pivots);
		}
	} else {
		return getRoot()->getIndex();
	}
}

/* //for counting comparisons in class binaryTree
int BinaryTree::helpInsertElement(int value, std::vector<int>* pivots, int* comparisons){ 
	if(getRoot()->isKey()){
		*comparisons = *comparisons + 1;
		if(value <= (*pivots)[getRoot()->getKey()]){ 
			return getLeft()->helpInsertElement(value, pivots, comparisons);
		} else {
			return getRight()->helpInsertElement(value, pivots, comparisons);
		}
	} else {
		return getRoot()->getIndex();
	}
}*/


/*
int BinaryTree::getNumberOfComparisons(){
	return m_comparisons;
} 
*/