/*
 * Evaluator.h
 *
 *  Created on: 28.01.2018
 *      Author: melanie
 */

#ifndef EVALUATOR_H_
#define EVALUATOR_H_

#include "Quicksort3Pivots.h"
#include "Quicksort.h"
#include "Permutations.h"
#include "PartitionerCount.h"
#include "Partitioner3Pivots.h"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <algorithm> //std::random_shuffle
#include <cmath> //std::pow
#include <random> //std::default_random_engine
#include <chrono> //std::chrono::system_clock
#include <cstdlib> //std::rand, std::srand
#include <string> //std::stoi
#include <time.h>
#include <boost/rational.hpp>

class Evaluator {
public:
	//writes the sum of comparisons to sort a random list of size len (n random lists)
	static void write_nr_of_comp_sort(int nPivots, int n, int64_t len);

	//writes the avg nr of comparisons to sort a random list of size len (sample size: n, reps times)
	static void write_avg_nr_of_comp_sort(int nPivots, int n, int64_t len, int reps);

	//writes the avg nr of comparisons to sort a random list of size len with 3 pivots(sample size: n, reps times)
	static void write_avg_nr_of_comp_sort_3_pivots(int n, int64_t len, int reps);

	//writes the number of comparisons/(n-nPivots) to partition a random list of size len  (n random lists)
	static void write_avg_nr_of_comp_partition(int nPivots, int n, int64_t len);

	//writes the number of comparisons/(n-3) to partition a random list of size len with 3 pivots (n times)
	static void write_avg_nr_of_comp_partition_3_pivots(int n, int64_t len);

	//writes the sum of comparisons to partition all possible lists of size len
	static void write_nr_of_comp_to_sort_all_possible_permutations(int nPivots, int64_t len);

	//writes the sum of comparisons to partition all possible lists of size len with 3 pivots
	static void write_nr_of_comp_to_sort_all_possible_permutations_3_pivots(int64_t len);

	//writes how often each sortingtree was used to partition a list of size len (n times)
	static void write_tree_usage_partition_3_pivots(int n, int64_t len);

private:
	// Disallow creating an instance of this object
	Evaluator();
	virtual ~Evaluator();

	int factorial(int n){
		if(n<0) return -1;
		int i = 1;
		while(n > 0){
			i *= n;
			n--;
		}
		return i;
	};
};

#endif /* EVALUATOR_H_ */
