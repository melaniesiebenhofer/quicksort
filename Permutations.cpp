#include "Permutations.h"


Permutations::Permutations(int64_t* startArray, int64_t arrayLength){
	m_array = new int64_t[arrayLength];
	m_c = new int64_t[arrayLength]; 
	for(int64_t i = 0; i < arrayLength; i++){
		m_array[i] = startArray[i];
		m_c[i] = 0;
	}
	m_i = 0;
	m_arrayLength = arrayLength;
	m_start = true;
	m_counter = 0;
	
	
}

Permutations::~Permutations()
{
	delete m_array;
	delete m_c;

}



/*
 * function returns true, if next permutation has been copied
 * into arrayForNextPermutation
 * returns false if there are no further permutations
 * 
 * */
bool Permutations::getNextPermutation(int64_t* arrayForNextPermutation){
	
	if(m_start){ //first permutation is the startArray
		m_start = false;
		copyArray(m_array, arrayForNextPermutation, m_arrayLength);
		m_counter++;
		return true;
	}
	
	//heap's algorithm for all permutations: https://en.wikipedia.org/wiki/Heap%27s_algorithm

	while(m_i < m_arrayLength){
		if(m_c[m_i] < m_i){
			if((m_i%2) == 0){
				swap(0, m_i);
			} else {
				swap(m_c[m_i], m_i);
			}
			m_c[m_i] += 1;
			m_i = 0;
			copyArray(m_array, arrayForNextPermutation, m_arrayLength);
			m_counter++;
			return true;
		} else {
			m_c[m_i] = 0;
			m_i++;
		}
	}
	
	return false;
	
	
}


void Permutations::swap(int64_t i, int64_t j){
	int64_t temp = m_array[i];
	m_array[i] = m_array[j];
	m_array[j] = temp;
}

void Permutations::copyArray(int64_t* arr, int64_t* copy, int64_t arrLength){
	for(int64_t i = 0; i < arrLength; i++){
		copy[i] = arr[i];
	}
}

int64_t Permutations::getNumberOfGeneratedPermutations(){
	return m_counter;
}