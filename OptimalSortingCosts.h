
#ifndef OPTIMALSORTINGCOSTS_H
#define OPTIMALSORTINGCOSTS_H

//#include <Rat.h>
#include <boost/rational.hpp>

class OptimalSortingCosts
{
public:
    boost::rational<uint64_t> optimalSortingCosts[5];
	OptimalSortingCosts();
	boost::rational<uint64_t>* getOptimalSortingCosts();
	int getSizeOfOptimalSortingCostsArray();
};


#endif //OPTIMALSORTINGCOSTS_H
