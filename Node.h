

#ifndef NODE_H
#define NODE_H

#include <vector>
#include  <stdint.h>
class Node
{


private:
	int64_t m_value;
	bool m_isKey; //0 key, else vector
public:
	Node(int64_t value, bool isKey);
	~Node();
	
	bool isKey();
	int64_t getKey();
	void setKey(int64_t key);
	bool isIndex();
	int64_t getIndex();
	void setIndex(int64_t index);
	Node* copy();
	

};

#endif // NODE_H