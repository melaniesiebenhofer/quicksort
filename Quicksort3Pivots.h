#ifndef QUICKSORT3PIVOTS_H
#define QUICKSORT3PIVOTS_H

#include "Quicksort.h" // Base class: Quicksort

class Quicksort3Pivots : public Quicksort
{
public:
	Quicksort3Pivots();
	virtual ~Quicksort3Pivots();

};

#endif // QUICKSORT3PIVOTS_H
