#include "Partitioner3Pivots.h"
#include <algorithm> //std::sort

//#include <stdio>
#include <iostream>

Partitioner3Pivots::Partitioner3Pivots()
{
	m_pivots = new std::vector<int64_t>();
	m_lists = new std::vector<int64_t>*[m_numberOfLists];
	for(int i = 0; i<m_numberOfLists; i++){
		m_lists[i] = new std::vector<int64_t>();
	}
	m_numberOfSortingSmallerLists = new uint64_t[2]; //[0]: sorting costs for 2 elements, [1]: 3 elements

	m_counterUsageOfTrees = new uint64_t[5];

	resetCounter();
	
}

Partitioner3Pivots::~Partitioner3Pivots()
{
	delete m_pivots;
	delete m_lists[0];
	delete m_lists[1];
	delete m_lists[2];
	delete m_lists[3];
	delete [] m_lists;
	delete m_numberOfSortingSmallerLists;
}

uint64_t Partitioner3Pivots::getNumberOfComparisons()
{
	return m_numberOfComparisons;
}

uint64_t* Partitioner3Pivots::getNumberOfSortingSmallerLists()
{
	return m_numberOfSortingSmallerLists;
}

std::vector<int64_t>* Partitioner3Pivots::getPivots()
{
	return m_pivots;
}

std::vector<int64_t>** Partitioner3Pivots::partition(int64_t* array, int64_t startIndex, int64_t endIndex)
{
	//std::cout << "partition number " << ++m_partitionCounter << std::endl;
	//std::cout << "sort subarray [" << startIndex << ", " << endIndex << "]\n";
	int64_t i; //iterator variable
	int64_t elem;
	int64_t arraysize = endIndex - startIndex + 1;
	m_pivots->clear(); //reset pivots
	m_lists[0]->clear(); //reset lists
	m_lists[1]->clear();
	m_lists[2]->clear();
	m_lists[3]->clear();
	
	if(arraysize <  3){ //if list to sort too small --> all in list 0
		for(i = startIndex; i<=endIndex; i++){
			m_lists[0]->push_back(array[i]);
		}
		if(arraysize == 2){
			std::sort(m_lists[0]->begin(), m_lists[0]->begin()+arraysize);
			m_numberOfSortingSmallerLists[0] += 1;
		}
		//if(arraysize == 3){
		//	std::sort(m_lists[0]->begin(), m_lists[0]->begin()+arraysize);
		//	m_numberOfSortingSmallerLists[1] += 1;
		//}
		return m_lists;
	}
	
	
	m_lVector[0] = 8;
	m_lVector[1] = 9;
	m_lVector[2] = 9;
	m_lVector[3] = 9;
	m_lVector[4] = 9;
	
	
	//choose pivots
	m_pivots->push_back(array[endIndex]);
	m_pivots->push_back(array[endIndex-1]);
	m_pivots->push_back(array[endIndex-2]);
	//sort pivots
	std::sort(m_pivots->begin(), m_pivots->begin()+3);
	m_numberOfSortingSmallerLists[1] += 1;
	int64_t pivot1 = m_pivots->at(0);
	int64_t pivot2 = m_pivots->at(1);
	int64_t pivot3 = m_pivots->at(2);
	//std::cout << "pivots: " << pivot1 << ", " << pivot2 << ", " << pivot3 << std::endl;
	//std::cout << "pivot2: " << pivot2 << std::endl;
	//std::cout << "pivot3: " << pivot3 << std::endl;
	
	
	//sort
	for(int64_t index = 0; index<arraysize-3; index++){
		
		//find tree
		m_sortingTree = 0;
		for(i = 1; i<5; i++){
			if(m_lVector[m_sortingTree]>=m_lVector[i]){
				m_sortingTree = i;
			}
		}
		
		elem = array[startIndex+index];
		switch(m_sortingTree){
			
			case 0:
				if(elem <= pivot2){
					if(elem <= pivot1){
						pushList0(elem);
						m_numberOfComparisons += 2;
					}
					else{
						pushList1(elem);
						m_numberOfComparisons += 2;
					}
				}
				else{
					if(elem <= pivot3){
						pushList2(elem);
						m_numberOfComparisons += 2;
					}
					else{
						pushList3(elem);
						m_numberOfComparisons += 2;
					}
				}
				break;
			
			case 1:
				if(elem <= pivot1){
					pushList0(elem);
					m_numberOfComparisons += 1;
				}
				else{
					if(elem<=pivot2){
						pushList1(elem);
						m_numberOfComparisons += 2;
					}
					else{
						if(elem<=pivot3){
							pushList2(elem);
							m_numberOfComparisons += 3;
						}
						else{
							pushList3(elem);
							m_numberOfComparisons += 3;
						}
					}
				}
			
				break;
				
			case 2:
				if(elem>pivot3){
					pushList3(elem);
					m_numberOfComparisons += 1;
				}
				else{
					if(elem>pivot2){
						pushList2(elem);
						m_numberOfComparisons += 2;
					}
					else{
						if(elem>pivot1){
							pushList1(elem);
							m_numberOfComparisons += 3;
						}
						else{
							pushList0(elem);
							m_numberOfComparisons += 3;
						}
					}
				}
			
				break;
				
			case 3:
				if(elem <= pivot1){
					pushList0(elem);
					m_numberOfComparisons += 1;
				}
				else{
					if(elem > pivot3){
						pushList3(elem);
						m_numberOfComparisons += 2;
					}
					else{
						if(elem <= pivot2){
							pushList1(elem);
							m_numberOfComparisons += 3;
						}
						else{
							pushList2(elem);
							m_numberOfComparisons += 3;
						}
					}
				}
				
				break;
			
			case 4:
				if(elem>pivot3){
					pushList3(elem);
					m_numberOfComparisons += 1;
				}
				else{
					if(elem <= pivot1){
						pushList0(elem);
						m_numberOfComparisons += 2;
					}
					else{
						if(elem <= pivot2){
							pushList1(elem);
							m_numberOfComparisons += 3;
						}
						else{
							pushList2(elem);
							m_numberOfComparisons += 3;
						}
					}
				}
				break;
		}
		m_counterUsageOfTrees[m_sortingTree] += 1;
	}
	
	
	
	return m_lists;
	
}

inline void Partitioner3Pivots::pushList0(int64_t elem){
	m_lists[0]->push_back(elem);
	m_lVector[0] += 2;
	m_lVector[1] += 1;
	m_lVector[2] += 3;
	m_lVector[3] += 1;
	m_lVector[4] += 2;
}

inline void Partitioner3Pivots::pushList1(int64_t elem){
	m_lists[1]->push_back(elem);
	m_lVector[0] += 2;
	m_lVector[1] += 2;
	m_lVector[2] += 3;
	m_lVector[3] += 3;
	m_lVector[4] += 3;
}

inline void Partitioner3Pivots::pushList2(int64_t elem){
	m_lists[2]->push_back(elem);
	m_lVector[0] += 2;
	m_lVector[1] += 3;
	m_lVector[2] += 2;
	m_lVector[3] += 3;
	m_lVector[4] += 3;
}

inline void Partitioner3Pivots::pushList3(int64_t elem){
	m_lists[3]->push_back(elem);
	m_lVector[0] += 2;
	m_lVector[1] += 3;
	m_lVector[2] += 1;
	m_lVector[3] += 2;
	m_lVector[4] += 1;
}
void Partitioner3Pivots::resetCounter()
{
	m_numberOfComparisons = 0;

	m_numberOfSortingSmallerLists[0] = 0;
	m_numberOfSortingSmallerLists[1] = 0;
	
	for(int i = 0; i < 5; i++){
		m_counterUsageOfTrees[i] = 0;
	}

	m_partitionCounter = 0;
}

uint64_t* Partitioner3Pivots::getCounterUsageOfTrees(){
	return m_counterUsageOfTrees;
}
