#include "Node.h"
#include <stdexcept>


Node::Node(int64_t value, bool isKey)
{
	m_value = value;
	m_isKey = isKey; 
}


Node::~Node()
{
}

bool Node::isKey(){
	return m_isKey;
}

int64_t Node::getKey(){
	if(!isKey()) return -1;
	return m_value;
}

void Node::setKey(int64_t key){
	if(isKey()){
		m_value = key;
	} else {
		throw  std::invalid_argument("node is an index - cannot set key");
	}
}

bool Node::isIndex(){
	return !Node::isKey();
}


int64_t Node::getIndex(){
	if(isKey()) return -1;
	return m_value;
}

void Node::setIndex(int64_t index){
	if(!Node::isKey()){
		m_value = index;
	} else {
		throw  std::invalid_argument("node is an key - cannot set index");
	}
}

Node* Node::copy(){
	bool isKeyValue = Node::isKey();
	int64_t value;
	if(isKeyValue){
		value = Node::getKey();
	} else {
		value = Node::getIndex();
	}
	Node* n = new Node(value, isKeyValue);
	return n;
}
