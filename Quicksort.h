#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "Partitioner.h"
#include "OptimalSortingCosts.h"

class Quicksort
{
	
protected:
	int m_numberOfPivots;
	Partitioner* m_partitioner;
	
	uint64_t* m_numberOfSortingSmallerLists;
	OptimalSortingCosts* m_optCosts;
	boost::rational<uint64_t>* m_optimalcosts;
	
	void helpSort(int64_t* array, int64_t startIndex, int64_t endIndex);
	bool noPivots; //for helpsort
	
public:
	Quicksort(int numberOfPivots);
	virtual ~Quicksort();
	
	uint64_t sort(int64_t* array, int64_t arrayLength); //returns #comparisons
	uint64_t* getNumberOfSortingSmallerLists(); //returns list with how many times smaller lists have been sorted
	boost::rational<uint64_t> getNumberOfComparisonsForSmallerLists();
	
	int getNumberOfPivots();
	void resetNumberOfSortingSmallerLists();
};

#endif // QUICKSORT_H
