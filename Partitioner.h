#ifndef PARTITIONER_H
#define PARTITIONER_H

#include <vector>
#include <stdint.h> //uint64_t

class Partitioner{
public:
	Partitioner();
	~Partitioner();
	
	//virtual int getNumberOfComparisonsOfPivots() = 0;
	//virtual int getNumberOfComparisonsSmallList(int size) = 0;
	
	virtual uint64_t* getNumberOfSortingSmallerLists() = 0;
	
	virtual std::vector<int64_t>** partition(int64_t* array, int64_t startIndex, int64_t endIndex) = 0;
	virtual std::vector<int64_t>* getPivots() = 0;
	
	virtual void resetCounter() = 0;
	virtual uint64_t getNumberOfComparisons() = 0;
};

#endif // PARTITIONER_H
