#include "OptimalSortingCosts.h"


OptimalSortingCosts::OptimalSortingCosts(){
	optimalSortingCosts[0] = boost::rational<uint64_t>(1,1); //costs for 2 elements
	optimalSortingCosts[1] = boost::rational<uint64_t>(16,6); //costs for 3 elements
	optimalSortingCosts[2] = boost::rational<uint64_t>(112,24);
	optimalSortingCosts[3] = boost::rational<uint64_t>(832,120);
	optimalSortingCosts[4] = boost::rational<uint64_t>(6896,720);
}

boost::rational<uint64_t>* OptimalSortingCosts::getOptimalSortingCosts(){
	return &optimalSortingCosts[0];
}

int OptimalSortingCosts::getSizeOfOptimalSortingCostsArray(){
	return int(sizeof(optimalSortingCosts))/int(sizeof(int));
}