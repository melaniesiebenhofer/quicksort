#include <UnitTest++/UnitTest++.h>
#include "../Permutations.h"
#include <vector>
#include <algorithm> //for std::sort
#include <cmath> //std::pow

TEST(TestPermutations_Permutations)
{
	int64_t c[5] = {1,2,3,4,5};
	Permutations* perm = new Permutations(c, 5);
	CHECK_EQUAL(0, perm->getNumberOfGeneratedPermutations());
	CHECK_EQUAL(true, perm->getNextPermutation(c));
	
	delete perm;
}

TEST(TestPermutations_copyArray)
{
	//private function
}

TEST(TestPermutations_getNextPermutation)
{
	int64_t c[5] = {1,2,3,4,5};
	int64_t num;
	std::vector<int64_t>* numbers = new std::vector<int64_t>{};
	Permutations* perm = new Permutations(c, 5);
	
	// check if all permutations are different
	// 1) transform each permutation to number and add to vector
	// 2) sort entries in vector
	// 3) check if there are two or more equal numbers in vector --> ERROR
	while(perm->getNextPermutation(c)){
		num = 0;
		for(int i = 0; i < 5; i++){
			num += c[i]*std::pow(10, i);
		}
		numbers->push_back(num);
	}
	
	bool duplicates = false;
	std::sort(numbers->begin(), numbers->end());
	for(std::vector<int64_t>::iterator it = numbers->begin() + 1; it!=numbers->end(); it++){
		if(*it == *(it-1)){
			duplicates = true;
		}
	}
	
	CHECK_EQUAL(false, duplicates);
	
	delete numbers;
	delete perm;
}

TEST(TestPermutations_getNumberOfGeneratedPermutations)
{
	int64_t c[5] = {1,2,3,4,5};
	Permutations* perm = new Permutations(c, 5);
	while(perm->getNextPermutation(c)){
	}
	CHECK_EQUAL(5*4*3*2*1, perm->getNumberOfGeneratedPermutations());
	
	delete perm;
}

TEST(TestPermutations_swap)
{
	//private function
}

TEST(TestPermutations_TildaPermutations)
{
}
