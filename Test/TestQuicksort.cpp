#include <UnitTest++/UnitTest++.h>
#include "../Quicksort.h"

TEST(TestQuicksort_Quicksort)
{
	Quicksort* qSort = new Quicksort(3);
	CHECK_EQUAL(3, qSort->getNumberOfPivots());
	CHECK_EQUAL(0,qSort->getNumberOfComparisonsForSmallerLists());
	delete qSort;
}

TEST(TestQuicksort_getNumberOfComparisonsForSmallerLists)
{
	Quicksort* qSort = new Quicksort(3);
	int64_t a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	qSort->sort(a, 21);
	CHECK_CLOSE(double(40)/double(3), boost::rational_cast<long double>(qSort->getNumberOfComparisonsForSmallerLists()), 0.00001); //5*(3elements) + 0*(2elements) = 5* 8/3 = 40/3
	
	int64_t b[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, /*18*/ 16, 12, 17, 19, 20, 10, 15, 5};
	qSort->sort(b, 21);
	CHECK_CLOSE(double(40)/double(3) + 1, boost::rational_cast<long double>(qSort->getNumberOfComparisonsForSmallerLists()), 0.00001); //5*(3elements) + 1*(2elements) = 5* 8/3 + 1= 40/3 +1
	
	delete qSort;
}

TEST(TestQuicksort_getNumberOfPivots)
{
	Quicksort* qSort = new Quicksort(3);
	CHECK_EQUAL(3, qSort->getNumberOfPivots());
	
	delete qSort;
}

TEST(TestQuicksort_getNumberOfSortingSmallerLists)
{
	Quicksort* qSort = new Quicksort(3);
	int64_t a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	qSort->sort(a, 21);
	CHECK_EQUAL(0, qSort->getNumberOfSortingSmallerLists()[0]);
	CHECK_EQUAL(5, qSort->getNumberOfSortingSmallerLists()[1]);
	
	int64_t b[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, /*18*/ 16, 12, 17, 19, 20, 10, 15, 5};
	qSort->sort(b, 21);
	CHECK_EQUAL(1, qSort->getNumberOfSortingSmallerLists()[0]);
	CHECK_EQUAL(5, qSort->getNumberOfSortingSmallerLists()[1]);
	
	delete qSort;
}

TEST(TestQuicksort_helpSort)
{
	//private function (gets called by sort)
}

TEST(TestQuicksort_resetNumberOfSortingSmallerLists)
{
	Quicksort* qSort = new Quicksort(3);
	int64_t a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	qSort->sort(a, 21);
	CHECK_EQUAL(0, qSort->getNumberOfSortingSmallerLists()[0]);
	CHECK_EQUAL(5, qSort->getNumberOfSortingSmallerLists()[1]);
	
	qSort->resetNumberOfSortingSmallerLists();
	CHECK_EQUAL(0, qSort->getNumberOfSortingSmallerLists()[0]);
	CHECK_EQUAL(0, qSort->getNumberOfSortingSmallerLists()[1]);
	
	delete qSort;
}

TEST(TestQuicksort_sort)
{
	Quicksort* qSort = new Quicksort(3);
	int64_t a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	int64_t count = qSort->sort(a, 21);
	CHECK_EQUAL(53, count);
	
	for(int i = 0; i<=20; i++){
		CHECK_EQUAL(i, a[i]);
	}
	
	
	
	int64_t b[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, /*18*/ 16, 12, 17, 19, 20, 10, 15, 5};
	count = qSort->sort(b, 21);
	CHECK_EQUAL(52, count);
	
	for(int i = 0; i<=16; i++){
		CHECK_EQUAL(i, b[i]);
	}
	CHECK_EQUAL(16, b[17]);
	CHECK_EQUAL(17, b[18]);
	CHECK_EQUAL(19, b[19]);
	CHECK_EQUAL(20, b[20]);
	
	
	delete qSort;
}

TEST(TestQuicksort_TildaQuicksort)
{
}
