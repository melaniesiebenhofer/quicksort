#include <UnitTest++/UnitTest++.h>
#include "../BinaryTree.h"
#include <map> //for getHeigths
#include <vector>

TEST(TestBinaryTree_BinaryTree)
{
	BinaryTree* tree = new BinaryTree(new Node(1,true), nullptr, nullptr);
	CHECK_EQUAL(1,tree->getRoot()->getKey());
	CHECK(nullptr==tree->getLeft());
	CHECK(nullptr==tree->getRight());
	delete tree;
}

TEST(TestBinaryTree_copy)
{
	BinaryTree* a = new BinaryTree(new Node(1,true), nullptr, nullptr);
	BinaryTree* b = a->copy();
	CHECK_EQUAL(a->getRepresentation(), b->getRepresentation());
	b->getRoot()->setKey(2);
	CHECK(b->getRoot()->getKey() != a->getRoot()->getKey());
	CHECK(a->getRoot()->getKey() == 1);
	CHECK(b->getRoot()->getKey() == 2);
	CHECK(a != b); //adress of a and b should be different
	delete a;
	delete b;
}

TEST(TestBinaryTree_getHeights)  
{
	BinaryTree* tree = new BinaryTree(new Node(1,true), 
									  new BinaryTree(new Node(0, true),
													 new BinaryTree(new Node(0, false), nullptr, nullptr),
													 new BinaryTree(new Node(1, false), nullptr, nullptr)
													 ),
									  new BinaryTree(new Node(2, true),
													 new BinaryTree(new Node(2, false), nullptr, nullptr),
													 new BinaryTree(new Node(3, true), 
																	new BinaryTree(new Node(3, false), nullptr, nullptr),
																	new BinaryTree(new Node(4, false), nullptr, nullptr)
																	)
													 )
									  );
	//				  ( 1 )						height 0
	//				/		\					
	//			(0) 	      (2) 				heigth 1
	//		   /  \			/	  \
	//		  L0  L1  	  L2        (3)			heigth 2
	//							   /   \
	//						     L3     L4		heigth 3
	//
	
	std::map<long int,int>* heights = tree->getHeights();
	
	std::map<long int,int>::iterator it = heights->begin();
	int i = 0;
	while(it != heights->end()){
		CHECK_EQUAL(i, it->first);
		if(i < 3){ //height = 2
			CHECK_EQUAL(2, it->second);
		} else { //3 and 4 on heigth 3
			CHECK_EQUAL(3, it->second);
		}
		it++;
		i++;
	}
	
	delete tree;
	delete heights;
	
}

TEST(TestBinaryTree_getKeys)
{
	BinaryTree* tree = new BinaryTree(new Node(1,true), 
									  new BinaryTree(new Node(0, true),
													 new BinaryTree(new Node(0, false), nullptr, nullptr),
													 new BinaryTree(new Node(1, false), nullptr, nullptr)
													 ),
									  new BinaryTree(new Node(2, true),
													 new BinaryTree(new Node(2, false), nullptr, nullptr),
													 new BinaryTree(new Node(3, true), 
																	new BinaryTree(new Node(3, false), nullptr, nullptr),
																	new BinaryTree(new Node(4, false), nullptr, nullptr)
																	)
													 )
									  );
	//				  ( 1 )						height 0
	//				/		\					
	//			(0) 	      (2) 				heigth 1
	//		   /  \			/	  \
	//		  L0  L1  	  L2        (3)			heigth 2
	//							   /   \
	//						     L3     L4		heigth 3
	//
	
	std::vector<long int> keys = tree->getKeys();
	CHECK(keys.size() == 4);//check #elements in keys
	
	//check if every key in keys:
	bool inKeys;
	for(int i = 0; i<=3; i++){
		inKeys = false;
		for(auto const& key: keys){
			 if(key == i){
				 inKeys = true;
			 }
		 }
		 CHECK(inKeys);
	}
	
	delete tree;
}

TEST(TestBinaryTree_getLeft)
{
	BinaryTree* tree = new BinaryTree(new Node(1, true),
									  new BinaryTree(new Node(0,true), nullptr, nullptr),
									  new BinaryTree(new Node(2, true), nullptr, nullptr)
									  );
	//				(1)
	//			   /   \
	//			 (0)   (2)
	
	CHECK_EQUAL(0,tree->getLeft()->getRoot()->getKey());
	CHECK(tree->getLeft()->getLeft() == nullptr);
	CHECK(tree->getLeft()->getRight() == nullptr);
	
	delete tree;
}

TEST(TestBinaryTree_getNumberOfComparisons)
{
	//not implemented for binaryTree (code in comments)
}

TEST(TestBinaryTree_getRepresentation)
{
	BinaryTree* tree = new BinaryTree(new Node(1,true), 
									  new BinaryTree(new Node(0, true),
													 new BinaryTree(new Node(0, false), nullptr, nullptr),
													 new BinaryTree(new Node(1, false), nullptr, nullptr)
													 ),
									  new BinaryTree(new Node(2, true),
													 new BinaryTree(new Node(2, false), nullptr, nullptr),
													 new BinaryTree(new Node(3, true), 
																	new BinaryTree(new Node(3, false), nullptr, nullptr),
																	new BinaryTree(new Node(4, false), nullptr, nullptr)
																	)
													 )
									  );
	//				  ( 1 )						height 0
	//				/		\					
	//			(0) 	      (2) 				heigth 1
	//		   /  \			/	  \
	//		  L0  L1  	  L2        (3)			heigth 2
	//							   /   \
	//						     L3     L4		heigth 3
	//
	
	//  representation:                 1    
	//					(             )     (					  )
	//					   ()  0  ()   		  ()  2  (          )  
	//												   ()  3  () 
	
	CHECK_EQUAL("(()0())1(()2(()3()))",tree->getRepresentation());
	
	
	BinaryTree* tree2 = new BinaryTree(new Node(1, true),
									  new BinaryTree(new Node(0,true), nullptr, nullptr),
									  new BinaryTree(new Node(2, true), nullptr, nullptr)
									  );
	//				(1)
	//			   /   \
	//			 (0)   (2)
	
	CHECK_EQUAL("(0)1(2)", tree2->getRepresentation());
	
	delete tree;
	delete tree2;
}

TEST(TestBinaryTree_getRepresentationWithLists)
{
	BinaryTree* tree = new BinaryTree(new Node(1,true), 
									  new BinaryTree(new Node(0, true),
													 new BinaryTree(new Node(0, false), nullptr, nullptr),
													 new BinaryTree(new Node(1, false), nullptr, nullptr)
													 ),
									  new BinaryTree(new Node(2, true),
													 new BinaryTree(new Node(2, false), nullptr, nullptr),
													 new BinaryTree(new Node(3, true), 
																	new BinaryTree(new Node(3, false), nullptr, nullptr),
																	new BinaryTree(new Node(4, false), nullptr, nullptr)
																	)
													 )
									  );
	//				  ( 1 )						height 0
	//				/		\					
	//			(0) 	      (2) 				heigth 1
	//		   /  \			/	  \
	//		  L0  L1  	  L2        (3)			heigth 2
	//							   /   \
	//						     L3     L4		heigth 3
	//
	
	//  representation:                      1    
	//					(                 )     (					 		 )
	//					   (L0)  0  (L1)   		  (L2)  2  (     		     )  
	//												           (L3)  3  (L4) 
	
	CHECK_EQUAL("((L0)0(L1))1((L2)2((L3)3(L4)))",tree->getRepresentationWithLists());
	
	
	BinaryTree* tree2 = new BinaryTree(new Node(1, true),
									  new BinaryTree(new Node(0,true), nullptr, nullptr),
									  new BinaryTree(new Node(2, true), nullptr, nullptr)
									  );
	//				(1)
	//			   /   \
	//			 (0)   (2)
	
	CHECK_EQUAL("(0)1(2)", tree2->getRepresentationWithLists());
	
	delete tree;
	delete tree2;	
}

TEST(TestBinaryTree_getRight)
{
	BinaryTree* tree = new BinaryTree(new Node(1, true),
									  new BinaryTree(new Node(0,true), nullptr, nullptr),
									  new BinaryTree(new Node(2, true), nullptr, nullptr)
									  );
	//				(1)
	//			   /   \
	//			 (0)   (2)
	
	CHECK_EQUAL(2,tree->getRight()->getRoot()->getKey());
	CHECK(tree->getRight()->getLeft() == nullptr);
	CHECK(tree->getRight()->getRight() == nullptr);
	
	delete tree;
}

TEST(TestBinaryTree_getRoot)
{
	BinaryTree* tree = new BinaryTree(new Node(1,false), nullptr, nullptr);
	CHECK_EQUAL(1, tree->getRoot()->getIndex());
}

TEST(TestBinaryTree_helpHeights)
{
	//method for getHeigths() --> already tested
}

TEST(TestBinaryTree_increaseIndices)
{
	BinaryTree* tree = new BinaryTree(new Node(1,true), 
									  new BinaryTree(new Node(0, true),
													 new BinaryTree(new Node(0, false), nullptr, nullptr),
													 new BinaryTree(new Node(1, false), nullptr, nullptr)
													 ),
									  new BinaryTree(new Node(2, true),
													 new BinaryTree(new Node(2, false), nullptr, nullptr),
													 new BinaryTree(new Node(3, true), 
																	new BinaryTree(new Node(3, false), nullptr, nullptr),
																	new BinaryTree(new Node(4, false), nullptr, nullptr)
																	)
													 )
									  );
	//				  ( 1 )						height 0
	//				/		\					
	//			(0) 	      (2) 				heigth 1
	//		   /  \			/	  \
	//		  L0  L1  	  L2        (3)			heigth 2
	//							   /   \
	//						     L3     L4		heigth 3
	// representation: ((L0)0(L1))1((L2)2((L3)3(L4)))
	
	tree->increaseIndices(3);
	CHECK_EQUAL("((L3)0(L4))1((L5)2((L6)3(L7)))", tree->getRepresentationWithLists());
	
	delete tree;
}

TEST(TestBinaryTree_increaseKeys)
{
		BinaryTree* tree = new BinaryTree(new Node(1,true), 
									  new BinaryTree(new Node(0, true),
													 new BinaryTree(new Node(0, false), nullptr, nullptr),
													 new BinaryTree(new Node(1, false), nullptr, nullptr)
													 ),
									  new BinaryTree(new Node(2, true),
													 new BinaryTree(new Node(2, false), nullptr, nullptr),
													 new BinaryTree(new Node(3, true), 
																	new BinaryTree(new Node(3, false), nullptr, nullptr),
																	new BinaryTree(new Node(4, false), nullptr, nullptr)
																	)
													 )
									  );
	//				  ( 1 )						height 0
	//				/		\					
	//			(0) 	      (2) 				heigth 1
	//		   /  \			/	  \
	//		  L0  L1  	  L2        (3)			heigth 2
	//							   /   \
	//						     L3     L4		heigth 3
	// representation: ((L0)0(L1))1((L2)2((L3)3(L4)))
	
	tree->increaseKeys(4);
	CHECK_EQUAL("((L0)4(L1))5((L2)6((L3)7(L4)))", tree->getRepresentationWithLists());
	
	delete tree;
}

TEST(TestBinaryTree_insertElement)
{
		BinaryTree* tree = new BinaryTree(new Node(1,true), 
									  new BinaryTree(new Node(0, true),
													 new BinaryTree(new Node(0, false), nullptr, nullptr),
													 new BinaryTree(new Node(1, false), nullptr, nullptr)
													 ),
									  new BinaryTree(new Node(2, true),
													 new BinaryTree(new Node(2, false), nullptr, nullptr),
													 new BinaryTree(new Node(3, true), 
																	new BinaryTree(new Node(3, false), nullptr, nullptr),
																	new BinaryTree(new Node(4, false), nullptr, nullptr)
																	)
													 )
									  );
	//				  ( 1 )						height 0
	//				/		\					
	//			(0) 	      (2) 				heigth 1
	//		   /  \			/	  \
	//		  L0  L1  	  L2        (3)			heigth 2
	//							   /   \
	//						     L3     L4		heigth 3
	
	

	std::vector<long int>* pivots = new std::vector<long int>{5,10,15,20};	// pivots: {5,10,15,20}
	
	CHECK_EQUAL(0, tree->insertElement(4, pivots));
	CHECK_EQUAL(0, tree->insertElement(5, pivots));
	CHECK_EQUAL(1, tree->insertElement(6, pivots));
	CHECK_EQUAL(2, tree->insertElement(13, pivots));
	CHECK_EQUAL(3, tree->insertElement(16, pivots));
	CHECK_EQUAL(4, tree->insertElement(999, pivots));
	
	delete tree;
	delete pivots;
}

TEST(TestBinaryTree_setLeft)
{
	BinaryTree* tree = new BinaryTree(new Node(1, true), nullptr, nullptr);
	BinaryTree* tree2 = tree->copy();
	tree2->increaseKeys(2);
	tree->setLeft(tree2);
	CHECK_EQUAL(3, tree->getLeft()->getRoot()->getKey());
	
	delete tree; //deletes tree2
}

TEST(TestBinaryTree_setRight)
{
	BinaryTree* tree = new BinaryTree(new Node(1, true), nullptr, nullptr);
	BinaryTree* tree2 = tree->copy();
	tree2->increaseKeys(2);
	tree->setRight(tree2);
	CHECK_EQUAL(3, tree->getRight()->getRoot()->getKey());
	
	delete tree;
}

TEST(TestBinaryTree_setRoot)
{
	Node* node1 = new Node(1, true);
	Node* node2 = new Node(2, true);
	BinaryTree* tree = new BinaryTree(node1, nullptr, nullptr);
	CHECK_EQUAL(1, tree->getRoot()->getKey());
	tree->setRoot(node2);
	CHECK_EQUAL(2, tree->getRoot()->getKey());
	
	delete node1;
	delete tree;
}

TEST(TestBinaryTree_TildaBinaryTree)
{
}
