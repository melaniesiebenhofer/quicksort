#include <UnitTest++/UnitTest++.h>
#include "../PartitionerCount.h"
#include <vector>

TEST(TestPartitionerCount_PartitionerCount)
{
	PartitionerCount* pCount = new PartitionerCount(3);
	CHECK_EQUAL(0, pCount->getNumberOfComparisons());
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); // |#comp2elements|#comp3elements|
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[1]);
	
	long int a[] = {0,1,2,3,4,5,6,7,8,9,10};
	pCount->partition(a, 0, 10);
	CHECK_EQUAL(3, pCount->getPivots()->size());
	
	delete pCount;
}

TEST(TestPartitionerCount_choosePivots)
{
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {0,1,2,3,4,5,6,7,8,9,10};
	pCount->partition(a, 0, 10);
	CHECK_EQUAL(3, pCount->getPivots()->size());
	CHECK_EQUAL(8, pCount->getPivots()->at(0));
	CHECK_EQUAL(9, pCount->getPivots()->at(1));
	CHECK_EQUAL(10, pCount->getPivots()->at(2));
	
	long int b[] = {0,9,2,3,8,5,6,10,4,1,7};
	pCount->partition(b, 0, 10);
	CHECK_EQUAL(1, pCount->getPivots()->at(0));
	CHECK_EQUAL(4, pCount->getPivots()->at(1));
	CHECK_EQUAL(7, pCount->getPivots()->at(2));
	
	delete pCount;
}

TEST(TestPartitionerCount_getNumberOfComparisons)
{
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {3,3,6,12,15,5,10};
	pCount->partition(a, 0, 6);
	CHECK_EQUAL(8, pCount->getNumberOfComparisons());
	
	
	pCount->resetCounter(); //counts per Array otherwise
	long int b[] = {16,17,12,4,10,5,15};
	pCount->partition(b, 0, 6);
	CHECK_EQUAL(10, pCount->getNumberOfComparisons());
	
	delete pCount;
}

TEST(TestPartitionerCount_getNumberOfSortingSmallerLists)
{
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {3,3,6,12,15,5,10};
	pCount->partition(a, 0, 6);
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	pCount->resetCounter();
	long int c[] = {1};
	pCount->partition(c, 0, 0);
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[1]);
	
	pCount->resetCounter();
	long int d[] = {1,2};
	pCount->partition(d, 0, 1);
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[1]);

	pCount->resetCounter();
	long int e[] = {1,2,3};
	pCount->partition(e, 0, 2);
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	
	pCount->resetCounter();
	long int b[] = {16,17,12,4,10,5,15};
	pCount->partition(b, 0, 6);
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	//without resetCounter
	long int f[] = {1,2};
	pCount->partition(f, 0, 1);
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	long int g[] = {1,2,3};
	pCount->partition(g, 0, 2);
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(2, pCount->getNumberOfSortingSmallerLists()[1]);
	
	delete pCount;
}

TEST(TestPartitionerCount_getPivots)
{
	//the same as in choosePivots()
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {0,1,2,3,4,5,6,7,8,9,10};
	pCount->partition(a, 0, 10);
	CHECK_EQUAL(3, pCount->getPivots()->size());
	CHECK_EQUAL(8, pCount->getPivots()->at(0));
	CHECK_EQUAL(9, pCount->getPivots()->at(1));
	CHECK_EQUAL(10, pCount->getPivots()->at(2));
	
	long int b[] = {0,9,2,3,8,5,6,10,4,1,7};
	pCount->partition(b, 0, 10);
	CHECK_EQUAL(1, pCount->getPivots()->at(0));
	CHECK_EQUAL(4, pCount->getPivots()->at(1));
	CHECK_EQUAL(7, pCount->getPivots()->at(2));
	
	delete pCount;
}

TEST(TestPartitionerCount_partition)
{
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	std::vector<long int>** lists = pCount->partition(a, 0, 20);
	CHECK_EQUAL(40, pCount->getNumberOfComparisons());
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	CHECK_EQUAL(5, lists[0]->size());
	CHECK_EQUAL(4, lists[1]->size());
	CHECK_EQUAL(4, lists[2]->size());
	CHECK_EQUAL(5, lists[3]->size());
	
	
	
	long int b[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	lists = pCount->partition(b, 0, 20);
	CHECK_EQUAL(40+40, pCount->getNumberOfComparisons());
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1+1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	CHECK_EQUAL(5, lists[0]->size());
	CHECK_EQUAL(4, lists[1]->size());
	CHECK_EQUAL(4, lists[2]->size());
	CHECK_EQUAL(5, lists[3]->size());
	
	delete pCount;
	delete [] lists;
}

TEST(TestPartitionerCount_resetCounter)
{
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	pCount->partition(a, 0, 20);
	CHECK_EQUAL(40, pCount->getNumberOfComparisons());
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	pCount->resetCounter();
	CHECK_EQUAL(0, pCount->getNumberOfComparisons());
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[1]);
	
	delete pCount;
}

TEST(TestPartitionerCount_resetLists)
{
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	std::vector<long int>** lists = pCount->partition(a, 0, 20);
	
	CHECK_EQUAL(5, lists[0]->size());
	CHECK_EQUAL(4, lists[1]->size());
	CHECK_EQUAL(4, lists[2]->size());
	CHECK_EQUAL(5, lists[3]->size());
	
	
	long int b[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 21, 22, 23, 10, 15, 5};
	lists = pCount->partition(b, 0, 23);
	
	CHECK_EQUAL(5, lists[0]->size());
	CHECK_EQUAL(4, lists[1]->size());
	CHECK_EQUAL(4, lists[2]->size());
	CHECK_EQUAL(8, lists[3]->size());
	
	delete pCount;
	delete [] lists;
}

TEST(TestPartitionerCount_resetNumberofSortingSmallerLists)
{
	//private function (gets called by resetCounter)
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {16, 0, 4, 9, 13, 1, 6, 8, 14, 2, 7, 11, 3, 18, 12, 17, 19, 20, 10, 15, 5};
	pCount->partition(a, 0, 20);
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(1, pCount->getNumberOfSortingSmallerLists()[1]);
	
	pCount->resetCounter();
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[0]); 
	CHECK_EQUAL(0, pCount->getNumberOfSortingSmallerLists()[1]);
	
	delete pCount;
}

TEST(TestPartitionerCount_resetPivots)
{
	PartitionerCount* pCount = new PartitionerCount(3);

	long int a[] = {0,1,2,3,4,5,6,7,8,9,10};
	pCount->partition(a, 0, 10);
	CHECK_EQUAL(3, pCount->getPivots()->size());
	CHECK_EQUAL(8, pCount->getPivots()->at(0));
	CHECK_EQUAL(9, pCount->getPivots()->at(1));
	CHECK_EQUAL(10, pCount->getPivots()->at(2));
	
	long int b[] = {0,9,2,3,8,5,6,10,4,1,7};
	pCount->partition(b, 0, 10);
	CHECK_EQUAL(3, pCount->getPivots()->size());
	CHECK_EQUAL(1, pCount->getPivots()->at(0));
	CHECK_EQUAL(4, pCount->getPivots()->at(1));
	CHECK_EQUAL(7, pCount->getPivots()->at(2));
	
	delete pCount;
}

TEST(TestPartitionerCount_TildaPartitionerCount)
{
}
