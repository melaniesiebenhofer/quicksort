#include <UnitTest++/UnitTest++.h>
#include "../ClassificationStrategy.h"

TEST(TestClassificationStrategy_ClassificationStrategy)
{
	//Test for 3 pivots:
	ClassificationStrategy* cStrategy = new ClassificationStrategy(3);
	CHECK_EQUAL(0, cStrategy->getNumberOfComparisons());
	CHECK_EQUAL(3, cStrategy->getNumberOfPivots());
	
	CHECK_EQUAL(5, cStrategy->getBinaryTrees()->size()); //5 classification trees for 3 pivots
	
	/* Bäume: */
	
	/* Baum 0:
	 * 
	 * 		(0)
	 *     /   \
	 * 	  0		(1)
	 * 			/  \
	 * 		  1     (2)
	 * 				/  \
	 * 			  2     3
	 * 
	 * */
	
	CHECK_EQUAL("(L0)0((L1)1((L2)2(L3)))", cStrategy->getBinaryTrees()->at(0)->getRepresentationWithLists()); //Baum 0
	
	/* Baum 1:
	 * 
	 * 			(0)
	 * 		  /     \
	 * 		0       (2)
	 * 			  /    \
	 * 			(1)     3
	 * 		   /   \
	 * 		 1      2
	 * 
	 * */
		
	CHECK_EQUAL("(L0)0(((L1)1(L2))2(L3))", cStrategy->getBinaryTrees()->at(1)->getRepresentationWithLists()); //Baum 1
	
	
	/* Baum 2:
	 * 					(1)
	 * 				 /      \
	 * 			   (0)       (2)
	 * 			  /   \     /   \
	 * 			0      1   2     3
	 * 
	 * */
	CHECK_EQUAL("((L0)0(L1))1((L2)2(L3))", cStrategy->getBinaryTrees()->at(2)->getRepresentationWithLists()); //Baum 2	
	
	
	/* Baum 3:
	 *  			(2)
	 * 			 /      \
	 * 		   (0)		 3
	 * 		 /    \
	 * 		0     (1)
	 * 			/    \
	 * 		   1      2
	 * 
	 * */
	
	CHECK_EQUAL("((L0)0((L1)1(L2)))2(L3)", cStrategy->getBinaryTrees()->at(3)->getRepresentationWithLists()); //Baum 3
	
	
	/* Baum 4:
	 * 					(2)
	 * 				  /    \
	 * 				(1)		3
	 * 			  /    \
	 * 			(0)		2
	 * 		   /   \
	 * 		  0     1
	 * 
	 * */
	CHECK_EQUAL("(((L0)0(L1))1(L2))2(L3)", cStrategy->getBinaryTrees()->at(4)->getRepresentationWithLists()); //Baum 4
	
	/*
	 * Matrix:  1 2 3 3 9
	 * 			1 3 3 2 9
	 * 			2 2 2 2 8
	 * 			2 3 3 1 9
	 * 			3 3 2 1 9
	 * 
	 * */
	
	CHECK_EQUAL("{9, 9, 8, 9, 9}", cStrategy->Vector());
	CHECK_EQUAL("{{1, 2, 3, 3, 9}, {1, 3, 3, 2, 9}, {2, 2, 2, 2, 8}, {2, 3, 3, 1, 9}, {3, 3, 2, 1, 9}}", cStrategy->Matrix());
	
	//first tree = tree 2
	CHECK_EQUAL("((L0)0(L1))1((L2)2(L3))", cStrategy->nextTree());
	
	delete cStrategy;
}

TEST(TestClassificationStrategy_chooseNextTree) 
{
	ClassificationStrategy* cStrategy = new ClassificationStrategy(3);
	std::vector<long int>* pivots = new std::vector<long int>{5,10,15}; //pivots must be sorted!!!
	// L0 |pivot5| L1 |pivot10| L2 |pivot15| L3
	
	CHECK_EQUAL(2, cStrategy->chooseNextTree()); // l={9, 9, 8, 9, 9}
	cStrategy->classifyElement(3, pivots);
	CHECK_EQUAL(0, cStrategy->chooseNextTree()); //l={10, 10, 10, 11, 12}
	cStrategy->classifyElement(3, pivots);
	CHECK_EQUAL(0, cStrategy->chooseNextTree()); // l={11, 12, 13, 15}
	cStrategy->classifyElement(6, pivots);
	CHECK_EQUAL(0, cStrategy->chooseNextTree()); // l={13, 14, 13, 16, 18}
	delete cStrategy;
	
	cStrategy = new ClassificationStrategy(3);
	
	cStrategy->classifyElement(16, pivots);
	CHECK_EQUAL(2, cStrategy->chooseNextTree()); // l={12, 11, 10, 10, 10}
	cStrategy->classifyElement(17, pivots);
	CHECK_EQUAL(3, cStrategy->chooseNextTree()); // l={15, 13, 12, 11, 11}
	cStrategy->classifyElement(12, pivots);
	CHECK_EQUAL(4, cStrategy->chooseNextTree()); //l={18, 16, 14, 14, 13}
	
	delete cStrategy;
}

TEST(TestClassificationStrategy_classifyElement)
{
	ClassificationStrategy* cStrategy = new ClassificationStrategy(3);
	std::vector<long int>* pivots = new std::vector<long int>{5,10,15}; //pivots must be sorted!!!
	// L0 |pivot5| L1 |pivot10| L2 |pivot15| L3
	CHECK_EQUAL(0,cStrategy->classifyElement(3, pivots));   
	CHECK_EQUAL(1,cStrategy->classifyElement(10, pivots));
	CHECK_EQUAL(0, cStrategy->classifyElement(3, pivots));
	CHECK_EQUAL(2, cStrategy->classifyElement(12, pivots));
	CHECK_EQUAL(3, cStrategy->classifyElement(16, pivots));
	CHECK_EQUAL(0, cStrategy->classifyElement(0, pivots));
	
	delete cStrategy;
	delete pivots;
}

TEST(TestClassificationStrategy_generateMatrix)
{
	//already tested in Constructor
}

TEST(TestClassificationStrategy_generateTrees)
{
	//already tested in Constructor
	
	CHECK_EQUAL(1, ClassificationStrategy(1).getBinaryTrees()->size());
	CHECK_EQUAL(2, ClassificationStrategy(2).getBinaryTrees()->size());
	CHECK_EQUAL(5, ClassificationStrategy(3).getBinaryTrees()->size());
	CHECK_EQUAL(14, ClassificationStrategy(4).getBinaryTrees()->size());
	CHECK_EQUAL(42, ClassificationStrategy(5).getBinaryTrees()->size());
}

TEST(TestClassificationStrategy_getBinaryTrees)
{
	// tested in Constructor and generateTrees
}

TEST(TestClassificationStrategy_getNumberOfComparisons) //counts until function-call of reset()
{
	ClassificationStrategy* cStrategy = new ClassificationStrategy(3);
	std::vector<long int>* pivots = new std::vector<long int>{5,10,15}; //pivots must be sorted!!!
	// L0 |pivot5| L1 |pivot10| L2 |pivot15| L3
	
	int count = 0;
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(3, pivots);
	count += 2; //in tree2 2comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(3, pivots);
	count += 1; //in tree 0  1comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(6, pivots);
	count += 2; //in tree0 2 comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(12, pivots);
	count += 3; //in tree0 3 comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	delete cStrategy;
	
	cStrategy = new ClassificationStrategy(3);
	count = 0;
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(16, pivots);
	count += 2; //in tree2 2 comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(17, pivots);
	count += 2; //in tree2 2 comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(12, pivots);
	count += 3; //in tree3 3 comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	cStrategy->classifyElement(4, pivots);
	count += 3; //in tree4 3 comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	delete cStrategy;
	delete pivots;
	
}

TEST(TestClassificationStrategy_getNumberOfPivots)
{
	ClassificationStrategy* cStrategy = new ClassificationStrategy(3);
	CHECK_EQUAL(3, cStrategy->getNumberOfPivots());
	delete cStrategy;
	
	cStrategy = new ClassificationStrategy(4);
	CHECK_EQUAL(4, cStrategy->getNumberOfPivots());
	delete cStrategy;
}

TEST(TestClassificationStrategy_printMatrix)
{
	//for debugging
}

TEST(TestClassificationStrategy_printNextTree)
{
	//for debugging
}

TEST(TestClassificationStrategy_printTrees)
{
	//for debugging
}

TEST(TestClassificationStrategy_printVector)
{
	//for debugging
}

TEST(TestClassificationStrategy_reset)
{
	ClassificationStrategy* cStrategy = new ClassificationStrategy(3);
	std::vector<long int>* pivots = new std::vector<long int>{5,10,15}; //pivots must be sorted!!!
	// L0 |pivot5| L1 |pivot10| L2 |pivot15| L3
	
	//some sorting:
	int count = 0;
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	cStrategy->classifyElement(3, pivots);
	count += 2; //in tree2 2comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	cStrategy->classifyElement(3, pivots);
	count += 1; //in tree 0  1comparisons
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	
	
	//TEST RESET:
	cStrategy->reset();
	count = 0;
	CHECK_EQUAL(count, cStrategy->getNumberOfComparisons());
	CHECK_EQUAL("{9, 9, 8, 9, 9}", cStrategy->Vector());
	
	delete cStrategy;
	delete pivots;
	
}

TEST(TestClassificationStrategy_updateVector)
{
	ClassificationStrategy* cStrategy = new ClassificationStrategy(3);
	std::vector<long int>* pivots = new std::vector<long int>{5,10,15}; //pivots must be sorted!!!
	// L0 |pivot5| L1 |pivot10| L2 |pivot15| L3
	
	CHECK_EQUAL("{9, 9, 8, 9, 9}", cStrategy->Vector());
	
	cStrategy->classifyElement(3, pivots);
	CHECK_EQUAL("{10, 10, 10, 11, 12}", cStrategy->Vector());
	
	cStrategy->classifyElement(3, pivots);
	CHECK_EQUAL("{11, 11, 12, 13, 15}", cStrategy->Vector());
	
	cStrategy->classifyElement(6, pivots);
	CHECK_EQUAL("{13, 14, 14, 16, 18}", cStrategy->Vector());
	
	cStrategy->classifyElement(12, pivots);
	CHECK_EQUAL("{16, 17, 16, 19, 20}", cStrategy->Vector());
	
	delete cStrategy;
	delete pivots;
}

TEST(TestClassificationStrategy_TildaClassificationStrategy)
{
}
