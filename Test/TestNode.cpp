#include <UnitTest++/UnitTest++.h>
#include "../Node.h" //for this project - compiler settings - include path - add: ../Quickstar
//#include <Node.cpp>

TEST(TestNode_Node)
{
	Node a = Node(1,true); //value = 1, iskey = true
	Node b = Node(2,false);
	CHECK_EQUAL(a.getKey(),1);
	CHECK_EQUAL(b.getIndex(),2);
}

TEST(TestNode_copy)
{
	Node* a = new Node(1,true);
	Node* b = a->copy();
	CHECK_EQUAL(a->isKey(), b->isKey());
	CHECK_EQUAL(a->getKey(), b->getKey());
	b->setKey(2);
	CHECK_EQUAL(a->getKey(), 1); //key of a stays unchanged
	CHECK(a!=b); //adress of a must not be adress of b
}

TEST(TestNode_getIndex)
{
	Node a = Node(100, false); //index
	Node b = Node(100, true); //key
	CHECK_EQUAL(a.getIndex(), 100);
	CHECK_EQUAL(b.getIndex(), -1); 
}

TEST(TestNode_getKey)
{
	Node a = Node(100, false); //index
	Node b = Node(100, true);  //key
	CHECK_EQUAL(a.getKey(), -1);
	CHECK_EQUAL(b.getKey(), 100);
}

TEST(TestNode_isIndex)
{
	Node a = Node(1, true);
	Node b = Node(2, false);
	CHECK(!a.isIndex());
	CHECK(b.isIndex());
}

TEST(TestNode_isKey)
{
	Node a = Node(1, true);
	Node b = Node(2, false);
	CHECK(a.isKey());
	CHECK(!b.isKey());
}

TEST(TestNode_setIndex)
{
	Node a = Node(0, false);
	a.setIndex(1);
	CHECK_EQUAL(a.getIndex(), 1);
}

TEST(TestNode_setKey)
{
	Node a = Node(0, true);
	a.setKey(1);
	CHECK_EQUAL(a.getKey(), 1);
}

//TEST(TestNode_TildaNode)
//{
//}
