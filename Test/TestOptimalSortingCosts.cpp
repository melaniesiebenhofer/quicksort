#include <UnitTest++/UnitTest++.h>
#include "../OptimalSortingCosts.h"

TEST(TestOptimalSortingCosts_OptimalSortingCosts)
{
}

TEST(TestOptimalSortingCosts_getOptimalSortingCosts)
{
	OptimalSortingCosts* opCosts = new OptimalSortingCosts();

	CHECK_EQUAL(1, 				 boost::rational_cast<long double>(opCosts->getOptimalSortingCosts()[0])         );
	CHECK_CLOSE(double(8)/3, 	 boost::rational_cast<long double>(opCosts->getOptimalSortingCosts()[1]), 0.00001);
	CHECK_CLOSE(double(112)/24,  boost::rational_cast<long double>(opCosts->getOptimalSortingCosts()[2]), 0.00001);
	CHECK_CLOSE(double(832)/120, boost::rational_cast<long double>(opCosts->getOptimalSortingCosts()[3]), 0.00001);
}

TEST(TestOptimalSortingCosts_getSizeOfOptimalSortingCostsArray)
{
}
