CXXFLAGS =	-O3 -std=c++0x -g -Wall -fmessage-length=0

OBJS =		Node.o \
			BinaryTree.o \
			ClassificationStrategy.o \
            OptimalSortingCosts.o \
            Partitioner.o \
            PartitionerCount.o \
            Partitioner3Pivots.o \
            Permutations.o \
            Quicksort.o \
            Quicksort3Pivots.o \
            Evaluator.o \
            main.o
            

LIBS =

TARGET =	Quicksort

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

build:	clean resultsDirectory $(TARGET)

test:
	cd ./Test && make test

clean:
	rm -f $(OBJS) $(TARGET)

resultsDirectory:
	mkdir -p ./Results
