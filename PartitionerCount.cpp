#include "PartitionerCount.h"
//#include "ClassificationStrategy.h"
//#include <vector>
#include <algorithm>    // std::sort
//#include <iostream>		// std::cout

PartitionerCount::PartitionerCount(int numberOfPivots)
{
	m_numberOfPivots = numberOfPivots;
	m_pivots = new std::vector<int64_t>();
	m_numberOfSortingSmallerLists = new uint64_t[numberOfPivots-1]; //no entry for lists of size 1
	resetNumberofSortingSmallerLists(); //set entries to 0
	m_numberOfLists = numberOfPivots + 1;
	m_lists = new std::vector<int64_t>*[m_numberOfLists];
	for(int i = 0; i<m_numberOfLists; i++){
		m_lists[i] = new std::vector<int64_t>();
	}
	m_strategy = new ClassificationStrategy(numberOfPivots);
	m_numberOfComparisons = 0;
	
	
	m_optCosts = new OptimalSortingCosts();
	m_optimalcosts = m_optCosts->getOptimalSortingCosts();
}

PartitionerCount::~PartitionerCount()
{
	delete m_strategy;
	delete m_optCosts;
}

uint64_t PartitionerCount::getNumberOfComparisons()
{
	return m_numberOfComparisons;
}

std::vector<int64_t>** PartitionerCount::partition(int64_t* array, int64_t startIndex, int64_t endIndex)
{
	int64_t arraySize = endIndex - startIndex + 1;
	m_strategy->reset(); //reset counter and l-vector 
	resetLists();
	resetPivots();
	//choose pivots --> more pivots than endIndex - startIndex? number of comp?
	//case: (endindex - startindex) < numberPivots

	if(arraySize < m_numberOfPivots){  //return all elements in first list (already sorted)
		for(int64_t i = startIndex; i<=endIndex; i++){
			m_lists[0]->push_back(array[i]);
		}
		
		std::sort(m_lists[0]->begin(), m_lists[0]->begin()+arraySize); //sortieren
		
		//m_numberOfComparisons += getNumberOfComparisonsSmallList(arraySize);
		if(arraySize >= 2){ //for 1 element #comp = 0
			m_numberOfSortingSmallerLists[arraySize-2] += 1; //list: |2|3|...|numberOfPivots|
		}
		
		return m_lists;
	}
	
	//case: enough elements (>= numberOfPivots)
	choosePivots(array, startIndex, endIndex);
	m_numberOfSortingSmallerLists[m_numberOfPivots-2] += 1; //for sorting pivots
	//m_numberOfComparisons += getNumberOfComparisonsOfPivots();
	//PROBLEM: removing pivots from array
	
	//for all elements in subarray:
	//((*lists)[classifyElement(elem, pivots)]))->push_back(elem)
	for(int64_t i = 0; i<arraySize-m_numberOfPivots; i++){ //last elements are pivots 
		(m_lists[m_strategy->classifyElement(array[startIndex+i], m_pivots)])->push_back(array[startIndex+i]);
	}
	
	
	m_numberOfComparisons += m_strategy->getNumberOfComparisons();
	
	
	return m_lists;
}

void PartitionerCount::resetCounter() {
	m_numberOfComparisons = 0;
	resetNumberofSortingSmallerLists();
}


void PartitionerCount::resetLists(){
	for(int i = 0; i<m_numberOfLists; i++){
		(m_lists[i])->clear();
	}
}

void PartitionerCount::resetPivots(){
	m_pivots->clear();
}


void PartitionerCount::choosePivots(int64_t* array, int64_t startIndex, int64_t endIndex){
	//PROBLEM: removing pivots from array
	//sort pivots --> number of comp?
	//insert smallest first  m_pivots->push_back(pivot)
	
	//strategy: choose the last elements in array --> won't be classified
	resetPivots();
	
	for(int i = 0; i < m_numberOfPivots; i++){
		m_pivots->push_back(array[endIndex-i]);
	}
	
	std::sort(m_pivots->begin(), m_pivots->begin()+m_numberOfPivots);
}

std::vector<int64_t>* PartitionerCount::getPivots(){
	return m_pivots;
}


/*

int PartitionerCount::getNumberOfComparisonsSmallList(int size){ //no integers!
	int comp = 0;
	switch(size){
		case 1: return 0;
		case 2: return 1;
		case 3: return (int) 8/3;
	}
	
	return 0;
}
 * */
 
 
void PartitionerCount::resetNumberofSortingSmallerLists(){
	 //set entries to 0
	 if(m_numberOfPivots>1){
		 for(int i = 0; i<m_numberOfPivots-1; i++){  //eg: numberOfPivots = 5:  List:  |2|3|4|5| (#comp 1 element = 0)
			 m_numberOfSortingSmallerLists[i] = 0;
		 }		 
	 }
}
 
 uint64_t* PartitionerCount::getNumberOfSortingSmallerLists(){
	 return m_numberOfSortingSmallerLists;
 }
 
 boost::rational<uint64_t> PartitionerCount::getNumberOfComparisonsForSmallerLists(){
	uint64_t* numberComparisons = getNumberOfSortingSmallerLists();

	if(m_numberOfPivots < 2 || m_numberOfPivots > m_optCosts->getSizeOfOptimalSortingCostsArray()+1 /* = 6 */){ //for #pivots>6 no results
		return 0;
	} else {
		boost::rational<uint64_t> sum = boost::rational<uint64_t>(0,1);
		for(int i = 0; i < m_numberOfPivots-1; i++){
			//sum =  sum + optimalcosts[i]*boost::rational<uint64_t>(numberComparisons[i],1);
			sum =  sum + m_optimalcosts[i]*numberComparisons[i];
		}
		//return boost::rational_cast<long double>(sum);
		return sum;
	}
}
